//parameter grade must be a string containing "5.(value)"
const getGradePage = (grade) => {
   let page = 0;
   //if grades are greater than 5.9 (5.10+)
   if (parseInt(grade.slice(2, 3)) === 1) {
      page = parseInt(grade.slice(3, 4)) + 1
   }
   return page;
}

//sorts array of grades and returns new array
const sortGrades = (gradesArray) => {
   gradesArray.sort((a, b) => {
      let c = a.split(" ");
      let d = b.split(" ");
      const grade1 = parseInt(c[0].replace("5.", ""));
      const grade2 = parseInt(d[0].replace("5.", ""));
      if (grade1 - grade2 < 0) return -1;
      else if (grade1 - grade2 > 0) return 1;

      if (c[1] < d[1]) return -1;
      else if (c[1] > d[1]) return 1;
      else return 0;
   })
   return gradesArray;
}

//reduces objects containing the same data to a single count
//reduces based on grade and sendType
const reduceGradeCounts = (array) => {
   let newArray = [];
   array.forEach(obj => {
      let index;
      const gradeNotAdded = newArray.every((grade, i) => {
         if (obj.grade === grade.grade && obj.sendType === grade.sendType) {
            index = i;
            return false;
         }
         return true;
      });
      if (gradeNotAdded) {
         newArray.push(obj);
      } else {
         newArray[index].count += obj.count;
      }
   });
   return newArray;
}

module.exports = {
   getGradePage,
   sortGrades,
   reduceGradeCounts,
}