const { forwardTo } = require("prisma-binding");
const cloneDeep = require("lodash.clonedeep");
const { getGradePage, sortGrades, reduceGradeCounts } = require("../helperFunctions");

const Query = {
  me(parent, args, ctx, info) {
    if (!ctx.request.userId) {
      return null;
    }
    return ctx.db.query.user({
      where: { id: ctx.request.userId }
    }, info);
  },

  project: forwardTo('db'),

  async projects(parent, args, ctx, info) {
    if (!ctx.request.userId) return [];
    const user = await ctx.db.query.user({
      where: { id: ctx.request.userId }
    });

    return ctx.db.query.projects({
      where: {
        user,
        name_contains: args.searchTerm,
        createdAt_gte: args.createdFrom,
        createdAt_lte: args.createdTo,
      },
      orderBy: args.orderBy,
    }, info)
  },

  async attempts(parent, args, ctx, info) {
    if (!ctx.request.userId) return [];
    const project = await ctx.db.query.project({
      where: { id: args.projectId }
    });

    return ctx.db.query.attempts({
      where: { project }
    }, info)
  },

  async climbs(parent, args, ctx, info) {
    if (!ctx.request.userId) return [];
    const user = await ctx.db.query.user({
      where: { id: ctx.request.userId }
    });

    return ctx.db.query.climbs({
      where: {
        user,
      },
      orderBy: 'createdAt_ASC',
    }, info)
  },

  journalEntry: forwardTo('db'),

  async journalEntries(parent, args, ctx, info) {
    if (!ctx.request.userId) return [];
    const user = await ctx.db.query.user({
      where: { id: ctx.request.userId }
    });

    return ctx.db.query.journalEntries({
      where: {
        user,
        OR: [{ comment_contains: args.searchTerm },
        { project: { name_contains: args.searchTerm } }]
      },
      orderBy: 'updatedAt_DESC',
    }, info)
  },

  //STAT DATA
  async totalVertical(parent, args, ctx, info) {
    const user = await ctx.db.query.user({
      where: { id: ctx.request.userId }
    },
      `{
    id
    climbs {
      id
      totalLength
    }
    projects {
      id
      totalLength
      completedDate
      attempts {
        id
      }
    }
  }
  `);

    let total = 0;

    const allClimbLengths = user.climbs.map(climb => climb.totalLength);
    if (allClimbLengths.length !== 0) {
      total = allClimbLengths.reduce((total, length) => (total + length));
    }
    const allProjectLengths = user.projects.map(climb => {
      if (climb.completedDate) {
        total += 1;
      }
      return climb.totalLength * climb.attempts.length;
    });
    if (allProjectLengths.length !== 0) {
      total += allProjectLengths.reduce((total, length) => (total + length));
    }

    return ({ value: total });
  },

  async pitchesThisMonth(parent, args, ctx, info) {
    const today = new Date();
    const month = today.getMonth() + 1;
    const year = today.getFullYear();

    const user = await ctx.db.query.user({
      where: { id: ctx.request.userId }
    });

    const climbs = await ctx.db.query.climbs({
      where: {
        user,
        createdAt_gte: `${year}-${month}`,
      },
      orderBy: 'createdAt_ASC',
    },
      `{
      id
      pitches
      }
      `);

    const projects = await ctx.db.query.projects({
      where: {
        user
      }
    },
      `{
        id
        pitches
        completedDate
        attempts {
          id
          createdAt
        }
      }
  `);

    let total = 0;
    const allPitches = climbs.map(climb => climb.pitches);
    if (allPitches.length !== 0) {
      total = allPitches.reduce((total, pitch) => (total + pitch))
    }
    projects.forEach(project => {
      const thisMonth = new Date(year, month - 1);
      const attemptPitches = project.attempts.map(attempt => {
        const attemptDate = new Date(attempt.createdAt);
        if (attemptDate > thisMonth) {
          return project.pitches;
        }
        else return 0;
      })
      if (attemptPitches.length !== 0) {
        total += attemptPitches.reduce((total, pitch) => (total + pitch));
      }
      if (project.completedDate) {
        const completedDate = new Date(project.completedDate);
        if (completedDate > thisMonth) {
          total += 1;
        }
      }
    });

    return ({ value: total });
  },

  async totalDaysThisYear(parent, args, ctx, info) {
    const today = new Date();
    const year = today.getFullYear();

    const user = await ctx.db.query.user({
      where: { id: ctx.request.userId }
    });

    const climbs = await ctx.db.query.climbs({
      where: {
        user,
        createdAt_gte: `${year}`,
      },
      orderBy: 'createdAt_ASC',
    },
      `{
      id
      createdAt
    }
    `);

    const projects = await ctx.db.query.projects({
      where: {
        user
      }
    },
      `{
        id
        completedDate
        attempts {
          id
          createdAt
        }
      }
  `);

    const allDays = climbs.map(climb => {
      const climbDate = new Date(climb.createdAt)
      return (`${climbDate.getMonth() + 1} ${climbDate.getDate()}`)
    });

    projects.forEach(project => {
      const attemptDays = project.attempts.map(attempt => {
        const attemptDate = new Date(attempt.createdAt)
        if (attemptDate > new Date(year)) {
          return (`${attemptDate.getMonth() + 1} ${attemptDate.getDate()}`);
        }
        else return (`removed`);
      })
      if (attemptDays.length !== 0) {
        allDays.concat(attemptDays);
      }
    });

    allDays.sort();
    //remove multiple day entries for the same day
    const filteredDays = allDays.filter((day, i, arr) => day !== arr[i - 1]);
    //remove day that was not within this year
    const i = filteredDays.findIndex(days => days === 'removed');
    if (i > -1) { filteredDays.splice(i, 1) }

    const total = filteredDays.length;
    return ({ value: total });
  },

  async highestRedpointGrade(parent, args, ctx, info) {
    const user = await ctx.db.query.user({
      where: { id: ctx.request.userId }
    });

    const climbs = await ctx.db.query.climbs({
      where: {
        user,
      },
      orderBy: 'grade_ASC',
    },
      `{
      id
      grade
      sendType
    }
    `);

    const projects = await ctx.db.query.projects({
      where: {
        user
      }
    },
      `{
      id
      grade
      completedDate
      }
  `);

    const redpoints = climbs.filter(climb =>
      climb.sendType === "Redpoint" || climb.sendType === "Onsight")
      .map(climb => climb.grade);

    const projectRedpoints = projects.filter(project => project.completedDate !== null)
      .map(project => project.grade);

    const allGrades = sortGrades(redpoints.concat(projectRedpoints));
    let highestGrade = null;
    if (allGrades.length > 0) {
      highestGrade = allGrades[allGrades.length - 1]
    }

    return ({ grade: highestGrade });
  },

  //CHART DATA
  async gradesChart(parent, args, ctx, info) {
    const user = await ctx.db.query.user({
      where: { id: ctx.request.userId }
    },
      `{
      id
      climbs {
        id
        grade
        pitches
        sendType
      }
      projects {
        id
        grade
        pitches
        completedDate
        attempts {
          id
          sendType
        }
      }
    }
    `);

    let climbsCount = [
      { seriesData: [] },
      { seriesData: [] },
      { seriesData: [] },
      { seriesData: [] },
      { seriesData: [] },
      { seriesData: [] },
      { seriesData: [] },
    ];
    user.climbs.forEach(climb => {
      const { sendType, grade, pitches } = climb;
      //separate climbing grades into pages (each grade is a page)
      const page = getGradePage(climb.grade);
      climbsCount[page].seriesData.push({ sendType, grade, count: pitches });
    });

    user.projects.forEach(project => {
      // add additional attempt for completion of project (not added to attempts)
      const page = getGradePage(project.grade);
      if (project.completedDate) {
        climbsCount[page].seriesData.push({ sendType: "Redpoint", grade: project.grade, count: 1 })
      }
      project.attempts.forEach(attempt =>
        climbsCount[page].seriesData.push({ sendType: attempt.sendType, grade: project.grade, count: project.pitches })
      );
    });

    climbsCount.forEach(set => {
      set.seriesData = reduceGradeCounts(set.seriesData);
    })
    return climbsCount;
  },

  async climbStyleChart(parent, args, ctx, info) {
    const user = await ctx.db.query.user({
      where: { id: ctx.request.userId }
    },
      `{
      id
      climbs {
        id
        grade
        climbStyle
        routeStyles
        createdAt
      }
      projects {
        id
        grade
        climbStyle
        routeStyles
        completedDate
        attempts {
          id
          createdAt
        }
      }
    }
    `);

    let climbsCount = [{ seriesData: [] }];
    user.climbs.forEach(climb => {
      const { grade, climbStyle, routeStyles } = climb;
      climbsCount[0].seriesData.push({ grade, climbStyle, routeStyles, date: new Date(climb.createdAt) });
    });

    user.projects.forEach(project => {
      const { grade, climbStyle, routeStyles } = project;
      // add additional attempt for completion of project (not added to attempts)
      if (project.completedDate) {
        climbsCount[0].seriesData.push({ grade, climbStyle, routeStyles, date: new Date(project.completedDate) })
      }
      project.attempts.forEach(attempt =>
        climbsCount[0].seriesData.push({ grade, climbStyle, routeStyles, date: new Date(attempt.createdAt) })
      );
    });

    climbsCount[0].seriesData.sort((a, b) => a.date - b.date);
    //if multiple years exist, separate seriesData by year
    if (climbsCount[0].seriesData.length > 1) {
      const firstYear = climbsCount[0].seriesData[0].date.getFullYear();
      const lastYear = climbsCount[0].seriesData[climbsCount[0].seriesData.length - 1].date.getFullYear();
      const numYears = lastYear - firstYear;
      if (numYears > 0) {
        //add more pages
        for (let i = 0; i < numYears; i++) {
          climbsCount.push({ seriesData: [] })
        }
        //add climbs to proper page
        climbsCount[0].seriesData.forEach(climb => {
          const page = climb.date.getFullYear() - firstYear;
          if (page > 0) {
            climbsCount[page].seriesData.push(cloneDeep(climb));
            climb.grade = undefined;
          }
        })
        //remove climbs from first page that were added to other pages
        climbsCount[0].seriesData = climbsCount[0].seriesData.filter(climb => climb.grade !== undefined)
      }
    }
    return climbsCount;
  },

};

module.exports = Query;
