const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { randomBytes } = require('crypto');
const { promisify } = require('util');
const { transport, resetEmail } = require('../mail');
require("dotenv").config({ path: "variables.env" });

const Mutations = {
  async signup(parent, args, ctx, info) {
    args.email = args.email.toLowerCase();
    if (args.password !== args.passwordConfirm) {
      throw new Error("Passwords do not match.")
    }
    delete args.passwordConfirm;
    const password = await bcrypt.hash(args.password, 10);
    const user = await ctx.db.mutation.createUser({
      data: {
        ...args,
        password,
      }
    }, info);

    const token = jwt.sign({ userId: user.id }, process.env.APP_SECRET);
    ctx.response.cookie('token', token, {
      domain: process.env.NODE_ENV === 'development' ? process.env.LOCAL_DOMAIN : process.env.APP_DOMAIN,
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24 * 365
    });
    return user;
  },

  async signin(parent, { email, password }, ctx, info) {
    email = email.toLowerCase();
    const user = await ctx.db.query.user({ where: { email } });
    if (!user) {
      throw new Error(`No user found with email ${email}.`);
    }

    const valid = await bcrypt.compare(password, user.password);
    if (!valid) {
      throw new Error(`Invalid password.`);
    }

    const token = jwt.sign({ userId: user.id }, process.env.APP_SECRET);
    ctx.response.cookie('token', token, {
      domain: process.env.NODE_ENV === 'development' ? process.env.LOCAL_DOMAIN : process.env.APP_DOMAIN,
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24 * 365
    });

    return user;
  },

  signout(parent, args, ctx, info) {
    ctx.response.clearCookie('token', {
      domain: process.env.NODE_ENV === 'development' ? process.env.LOCAL_DOMAIN : process.env.APP_DOMAIN
    });
    return { message: "You are signed out." };
  },

  async requestReset(parent, args, ctx, info) {
    args.email = args.email.toLowerCase();
    const user = await ctx.db.query.user({ where: { email: args.email } });
    if (!user) {
      throw new Error(`No user found with email ${args.email}.`);
    }
    //create reset token and upload to database
    //uses randomBytes to cryptify, randomBytes should be run async
    //runs on callback so uses promisify to change callback into promise
    const randomBytesPromisified = promisify(randomBytes);
    const resetToken = (await randomBytesPromisified(20)).toString('hex');
    const resetTokenExpiry = Date.now() + 3600000; //1 hr from now
    const res = await ctx.db.mutation.updateUser({
      where: { email: args.email },
      data: { resetToken, resetTokenExpiry }
    });

    //email user reset token
    const mailRes = await transport.sendMail({
      from: 'kiesha.herman@gmail.com',
      to: user.email,
      subject: 'iClimb Tracker: Password Reset',
      html: resetEmail(`Looks like you forgot your password. No problem.
      \n\n <a href="${process.env.FRONTEND_URL}/reset?resetToken=${resetToken}">
        Click here to reset.</a>`)
    });

    return { message: "Password reset sent to email." }
  },

  async resetPassword(parent, args, ctx, info) {
    if (args.password !== args.confirmPassword) {
      throw new Error(`Passwords do not match!`);
    }
    //check if reset token is legit or expired
    //query by userS to use more robust searching, get first user
    const [user] = await ctx.db.query.users({
      where: {
        resetToken: args.resetToken,
        resetTokenExpiry_gte: Date.now() - 3600000,
      }
    });
    if (!user) {
      throw new Error('This token is either invalid or expired.');
    }

    const password = await bcrypt.hash(args.password, 10);
    const updatedUser = await ctx.db.mutation.updateUser({
      where: { email: user.email },
      data: { password, resetToken: null, resetTokenExpiry: null }
    });

    const token = jwt.sign({ userId: updatedUser.id }, process.env.APP_SECRET);
    ctx.response.cookie('token', token, {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24 * 365
    });

    return updatedUser;
  },

  async createClimb(parent, args, ctx, info) {
    if (!ctx.request.userId) {
      throw new Error('You must be logged in to add a climb.')
    }
    const climb = await ctx.db.mutation.createClimb(
      {
        data: {
          ...args,
          user: {
            connect: {
              id: ctx.request.userId
            },
          },
          routeStyles: { set: args.routeStyles },
        }
      },
      info
    );
    return climb;
  },

  async createProject(parent, args, ctx, info) {
    if (!ctx.request.userId) {
      throw new Error('You must be logged in to add a project.')
    }
    const project = await ctx.db.mutation.createProject(
      {
        data: {
          ...args,
          user: {
            connect: {
              id: ctx.request.userId
            },
          },
          routeStyles: { set: args.routeStyles },
        }
      },
      info
    );
    return project;
  },

  async updateProject(parent, args, ctx, info) {
    const updates = { ...args };
    delete updates.id;
    delete updates.routeStyles;
    return ctx.db.mutation.updateProject(
      {
        data: {
          ...updates,
          routeStyles: { set: args.routeStyles },
        },
        where: { id: args.id },
      },
      info
    );
  },

  async updateProjectCompletedDate(parent, args, ctx, info) {
    return ctx.db.mutation.updateProject(
      {
        data: {
          completedDate: args.completedDate,
        },
        where: { id: args.id },
      },
      info
    );
  },

  async createJournalEntryForClimb(parent, args, ctx, info) {
    const entry = await ctx.db.mutation.createJournalEntry(
      {
        data: {
          user: {
            connect: {
              id: ctx.request.userId
            }
          },
          climb: {
            connect: {
              id: args.climbId
            }
          },
          comment: args.comment
        }
      }, info
    );
    return entry;
  },

  async createAttempt(parent, args, ctx, info) {
    const data = { ...args };
    delete data.projectId;
    const attempt = await ctx.db.mutation.createAttempt(
      {
        data: {
          ...data,
          project: {
            connect: {
              id: args.projectId
            },
          },
        }
      },
      info
    );
    return attempt;
  },

  async createJournalEntryForAttempt(parent, args, ctx, info) {
    const entry = await ctx.db.mutation.createJournalEntry(
      {
        data: {
          user: {
            connect: {
              id: ctx.request.userId
            }
          },
          project: {
            connect: {
              id: args.projectId
            }
          },
          attempt: {
            connect: {
              id: args.attemptId
            }
          },
          comment: args.comment
        }
      }, info
    );
    return entry;
  },

  async createJournalEntry(parent, args, ctx, info) {
    const je = await ctx.db.mutation.createJournalEntry({
      data: {
        user: {
          connect: {
            id: ctx.request.userId
          }
        },
        comment: args.comment
      }
    }, info);

    if (args.projectId !== '') {
      const projectId = await ctx.db.query.project({
        where: { id: args.projectId }
      }, `{ id }`);

      await ctx.db.mutation.updateJournalEntry({
        data: {
          project: {
            connect: {
              id: projectId.id
            }
          }
        },
        where: { id: je.id },
      }, info
      );
    }

    return je;
  },

  async updateJournalEntry(parent, args, ctx, info) {
    return ctx.db.mutation.updateJournalEntry(
      {
        data: {
          comment: args.comment,
        },
        where: { id: args.id },
      },
      info
    );
  },

  async deleteProject(parent, args, ctx, info) {
    const project = await ctx.db.query.project({
      where: { id: args.id }
    }, `{ id user { id }}`);

    if (!project.user.id === ctx.request.userId) {
      throw new Error('You are not the owner of this project.');
    }
    return ctx.db.mutation.deleteProject({ where: { id: args.id } }, info);
  }
};

module.exports = Mutations;