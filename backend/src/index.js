const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');
const compression = require('compression');
require("dotenv").config({ path: "variables.env" });
const createServer = require("./createServer");
const db = require("./db");

const server = createServer();

server.express.use(cookieParser());

server.express.use((req, res, next) => {
  const { token } = req.cookies;
  if (token) {
    jwt.verify(token, process.env.APP_SECRET, function (err, decoded) {
      if (err) {
        console.log(err)
      } else {
        req.userId = decoded.userId;
      }
    });
  }
  next();
})

server.use(compression());

server.start(
  {
    cors: {
      credentials: true,
      origin: [process.env.FRONTEND_URL, 'http://www.iclimbtracker.com']
    }
  },
  deets => {
    console.log(`Server is running on port http://localhost:${deets.port}`);
  }
);

