const months = [
   "January",
   "February",
   "March",
   "April",
   "May",
   "June",
   "July",
   "August",
   "September",
   "October",
   "November",
   "December"
];

//takes a UTCDate and array of date format specifications,
//returns a string with specified formatted date
//parameters: UTCDate, specified date format (day, month, year)
//returns: string
const formatDate = (utcDate, ...vars) => {
   const allowedVars = ["day", "month", "year"];
   if (!vars.length) {
      throw "Missing at least one second argument"
   }
   vars.forEach(one => {
      if (allowedVars.every(date => date !== one)) {
         throw "Second parameter(s) must be any of 'day', 'month', 'year'"
      }
   })

   const date = new Date(utcDate);
   const transformedDate = {
      month: months[date.getMonth()],
      day: date.getDate(),
      year: date.getFullYear(),
   }

   let dateStr = '';
   vars.forEach(one => {
      const date = '' + transformedDate[one] + ' ';
      dateStr += date
   });
   return dateStr;
}

export { formatDate, months };