import Swal from 'sweetalert2';

const warningQuestion = async (question, note) => {
   const result = await Swal.fire({
      title: question,
      text: note,
      type: 'warning',
      confirmButtonText: "Yes",
      showCancelButton: true,
      cancelButtonText: "No",
      reverseButtons: true,
   });
   if (result.value) {
      return true;
   } else {
      return false;
   }
}

const sendQuestion = async () => {
   const result = await Swal.fire({
      title: 'Did you send your project!?',
      type: 'question',
      confirmButtonText: "YES!",
      showCancelButton: true,
      cancelButtonText: "Not yet",
      reverseButtons: true,
   });
   if (result.value) {
      Swal.fire({
         title: '&#127867 Hell Yeah!! Way to go!',
         text: 'Time to celebrate!',
         timer: 2200,
         showConfirmButton: false,
      });
      return true;
   } else {
      return false;
   }

}

const sendAttemptQuestion = async () => {
   const result = await Swal.fire({
      title: 'Did you send your project!?',
      type: 'question',
      confirmButtonText: "YES!",
      showCancelButton: true,
      cancelButtonText: "Not yet",
      reverseButtons: true,
   });
   if (result.value) {
      Swal.fire({
         title: 'So Awesome!',
         text: 'Check it off your projects list!',
         timer: 2200,
         showConfirmButton: false,
      });
      return true;
   } else {
      return false;
   }
}

const noChangesMadeNotification = () => {
   Swal.fire({
      title: 'No changes were made. Attempt not added.',
      timer: 2200,
      showConfirmButton: false,
   });
}

const saveNotification = (title) => {
   Swal.fire({
      title: title,
      timer: 2000,
      showConfirmButton: false,
      type: "success",
   });
}

export {
   warningQuestion,
   sendQuestion,
   sendAttemptQuestion,
   noChangesMadeNotification,
   saveNotification,
}
