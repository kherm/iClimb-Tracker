const grades = [
  "5.6",
  "5.7",
  "5.8",
  "5.9",
  "5.10 a",
  "5.10 b",
  "5.10 c",
  "5.10 d",
  "5.11 a",
  "5.11 b",
  "5.11 c",
  "5.11 d",
  "5.12 a",
  "5.12 b",
  "5.12 c",
  "5.12 d",
  "5.13 a",
  "5.13 b",
  "5.13 c",
  "5.13 d",
  "5.14 a",
  "5.14 b",
  "5.14 c",
  "5.14 d",
  "5.15 a",
  "5.15 b",
  "5.15 c",
  "5.15 d"
];

//takes an array of grades and sorts them from lowest to highest
//parameters: 1 array with contents containing only grade entries
const sortGrades = (gradesArray) => {
  gradesArray.sort((a, b) => {
    let c = a.split(" ");
    let d = b.split(" ");
    const grade1 = parseInt(c[0].replace("5.", ""));
    const grade2 = parseInt(d[0].replace("5.", ""));
    if (grade1 - grade2 < 0) return -1;
    else if (grade1 - grade2 > 0) return 1;

    if (c[1] < d[1]) return -1;
    else if (c[1] > d[1]) return 1;
    else return 0;
  })
  return gradesArray;
}

const sendTypes = ["Top Rope", "Redpoint", "Onsight"];

const climbStyle = ["Trad", "Sport"];

const routeStyles = [
  "Crack",
  "Crimpy",
  "Technical",
  "Pumpy",
  "Offwidth",
  "Fingers",
  "OffFingers",
  "Hands",
  "OffHands",
  "Fists",
  "OffFists",
]

export {
  grades,
  sendTypes,
  climbStyle,
  routeStyles,
  sortGrades
};
