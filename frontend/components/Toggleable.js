import React, { Component } from 'react'

export default class Toggleable extends Component {
   state = {
      show: this.props.show || false
   }

   toggle = () => {
      this.setState(prevState => ({ show: !prevState.show }));
   }

   render() {
      return this.props.children(this.state.show, this.toggle);
   }
}
