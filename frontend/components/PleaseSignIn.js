import React, { Component } from 'react';
import styled from 'styled-components';
import { Query } from 'react-apollo';
import { CURRENT_USER_QUERY } from './User';
import Toggleable from './Toggleable';
import Signin from './Signin';
import Signup from './Signup';
import { theme } from '../components/styles/GlobalStyles'

const Wrapper = styled.div`
  display: grid;
  justify-content: center;
  * {
    min-width: 340px;
  }
`;

const Button = styled.button`
  background-color: ${theme.yellow};
  color: ${theme.green};
  padding: 1rem;
  border-radius: 10px;
  box-shadow: 1px 1px 1px grey;
  margin: 1rem;
  border: 1px ${theme.green} solid;
  font-weight: bold;
  cursor: pointer;
`;

const PleaseSignIn = (props) => (
   <Query query={CURRENT_USER_QUERY}>
      {({ data, loading, refetch }) => {
         if (loading) return <p>Loading...</p>;
         if (!data.me) {
            return (
               <Toggleable>
                  {(show, toggle) => (
                     <Wrapper>
                        <Button onClick={toggle}>
                           {show ? "Already a member?" : "Create a new account"}
                        </Button>
                        {show ?
                           <Signup onLogin={() => refetch()} /> :
                           <Signin onLogin={() => refetch()} />}
                     </Wrapper>
                  )}
               </Toggleable>
            )
         }
         return props.children;
      }}
   </Query>
);

export default PleaseSignIn;