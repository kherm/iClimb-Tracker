import React from 'react';
import { ApolloConsumer, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { CURRENT_USER_QUERY } from './User';

const SIGNOUT_MUTATION = gql`
   mutation SIGNOUT_MUTATION {
      signout {
         message
      }
   }
`;

const Signout = () => (
   <ApolloConsumer>
      {(client) => (
         <Mutation
            mutation={SIGNOUT_MUTATION}
            refetchQueries={[{ query: CURRENT_USER_QUERY }]}
            awaitRefetchQueries={true}
            onCompleted={() => {
               localStorage.clear();
               sessionStorage.clear();
               client.clearStore().then(() => {
                  client.resetStore();
               });
            }}
         >
            {
               (signout) => <button onClick={async () => {
                  await signout();
                  window.location.reload();
               }}>Logout</button>
            }
         </Mutation>
      )}
   </ApolloConsumer>
);

export default Signout;
