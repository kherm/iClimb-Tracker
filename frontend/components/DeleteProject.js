import React, { Component } from 'react';
import styled from 'styled-components';
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import router from 'next/router';
import { theme } from './styles/GlobalStyles';
import { LOAD_USER_PROJECTS_QUERY } from './ProjectsPage/RouteList';
import { warningQuestion, saveNotification } from '../lib/Notification';

const DELETE_PROJECT_MUTATION = gql`
   mutation DELETE_PROJECT_MUTATION($id: ID!) {
      deleteProject(id: $id) {
         id
      }
   }
`;

const Button = styled.div`
   .delete {
      background-color: white;
      color: ${theme.green};
      padding: 0.5rem;
      font-size: 1.2rem;
      text-transform: uppercase;
   }
`;


class DeleteProject extends Component {
   update = (cache, payload) => {
      const data = cache.readQuery({ query: LOAD_USER_PROJECTS_QUERY });
      data.projects = data.projects.filter(project => project.id !== payload.data.deleteProject.id);
      cache.writeQuery({ query: LOAD_USER_PROJECTS_QUERY, data });
   };

   render() {
      return (
         <Mutation
            mutation={DELETE_PROJECT_MUTATION}
            variables={{ id: this.props.id }}
            update={this.update}
         >
            {(deleteProject) => (
               <Button>
                  <button
                     type="button"
                     className="delete"
                     onClick={async e => {
                        const isDelete = await warningQuestion(
                           'Are you sure you want to delete this project?',
                           'This cannot be reversed.'
                        );
                        if (isDelete) {
                           deleteProject()
                              .then(res => {
                                 saveNotification('Project Deleted.');
                                 router.push('/projects');
                              })
                              .catch(error => {
                                 alert(error.message);
                              });
                        }
                     }}
                  >🗑️ Delete Project</button>
               </Button>
            )}
         </Mutation>
      );
   }
}

export default DeleteProject;
