import React from "react";
import Link from "next/link";
import { withRouter } from "next/router";
import { NavList } from "./styles/NavStyles";
import User from './User';
import Signout from './Signout';

const Nav = props => {
  return (
    <User>
      {({ data: { me } }) => (
        me && (
          <NavList>
            <Link href="/stats">
              <a
                className={props.router.pathname === "/stats" ? "active" : "default"}
              >
                Stats
              </a>
            </Link>
            <Link href="/projects">
              <a
                className={
                  props.router.pathname === "/projects" ? "active" : "default"
                }
              >
                Projects
              </a>
            </Link>
            <Link href="/journal">
              <a
                className={
                  props.router.pathname === "/journal" ? "active" : "default"
                }
              >
                Journal
              </a>
            </Link>

            <Signout />
          </NavList>
        )
      )}
    </User>
  );
};

export default withRouter(Nav);
