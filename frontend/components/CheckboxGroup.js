import React, { Component } from 'react';
import PropTypes from "prop-types";
import styled from "styled-components";

const Wrapper = styled.div`
   display: grid;
   grid-template-columns: repeat(auto-fit, minmax(120px,1fr));
`;

const Label = styled.label`
   display: grid;
   grid-template-columns: 1fr 2fr;
   justify-items: start;

   input, p {
      margin: 0;
   }
`;

export default class CheckboxGroup extends Component {
   static propTypes = {
      options: PropTypes.array
   }

   state = {
      styles: this.props.preSelect || [],
   };

   handleClick = async (e, style) => {
      if (e.target.checked) {
         await this.setState(prevState => ({ styles: [...prevState.styles, style] }));
      } else {
         const newList = this.state.styles.filter(item => item !== style);
         await this.setState({ styles: newList })
      }
      this.props.onChange(this.state.styles);
   }

   render() {
      return (
         <Wrapper>
            {this.props.options.map((style) => (
               <Label key={style}>
                  <input
                     type="checkbox"
                     onChange={e => this.handleClick(e, style)}
                     defaultChecked={this.state.styles.includes(style)}
                  />
                  <p>{style}</p>
               </Label>
            ))}
         </Wrapper>
      )
   }
}
