import React, { Component } from 'react'
import { Mutation } from 'react-apollo';
import Router from 'next/router';
import gql from 'graphql-tag';
import Form from './styles/Form'
import DisplayError from './DisplayError';
import * as queries from './StatsPage/StatsContainer';
import { LOAD_USER_JOURNAL_QUERY } from './JournalPage/JournalList';
import { LOAD_USER_PROJECTS_QUERY } from './ProjectsPage/RouteList';
import { CURRENT_USER_QUERY } from './User';

const SIGNUP_MUTATION = gql`
   mutation SIGNUP_MUTATION(
      $email: String!,
      $password: String!,
      $passwordConfirm: String!,
      $name: String!
      ) {
      signup(
         email: $email,
         password: $password,
         passwordConfirm: $passwordConfirm,
         name: $name
         ) {
         id
         name
      }
   }
`;

export default class Signup extends Component {
   state = {
      email: '',
      password: '',
      passwordConfirm: '',
      name: '',
   }

   saveToState = (e) => {
      this.setState({ [e.target.name]: e.target.value });
   }

   render() {
      return (
         <Mutation
            mutation={SIGNUP_MUTATION}
            variables={this.state}
            refetchQueries={[
               { query: queries.USER_HIGHEST_GRADE_QUERY },
               { query: queries.USER_PITCHES_MONTH_QUERY },
               { query: queries.USER_TOT_DAYS_YEAR_QUERY },
               { query: queries.USER_TOT_VERTICAL_QUERY },
               { query: queries.GRADES_VS_COUNTS_QUERY },
               { query: queries.STYLE_VS_TIME_QUERY },
               { query: LOAD_USER_JOURNAL_QUERY },
               { query: CURRENT_USER_QUERY },
               { query: LOAD_USER_PROJECTS_QUERY },
            ]}
            awaitRefetchQueries={true}
         >
            {(signup, { error, loading }) => {
               return (
                  <Form method="post" onSubmit={async e => {
                     e.preventDefault();
                     await signup();
                     await this.props.onLogin();
                     window.location.reload();
                     Router.replace('/stats');
                  }}>
                     <fieldset disabled={loading} aria-busy={loading}>
                        <h2>Create an Account</h2>
                        <DisplayError error={error} />
                        <input
                           type="text"
                           name="name"
                           placeholder="Name"
                           value={this.state.name}
                           onChange={this.saveToState}
                           required
                        />
                        <input
                           type="text"
                           name="email"
                           placeholder="Email"
                           value={this.state.email}
                           onChange={this.saveToState}
                           required
                        />
                        <input
                           type="password"
                           name="password"
                           placeholder="Password"
                           value={this.state.password}
                           onChange={this.saveToState}
                           required
                        />
                        <input
                           type="password"
                           name="passwordConfirm"
                           placeholder="Re-enter Password"
                           value={this.state.passwordConfirm}
                           onChange={this.saveToState}
                           required
                        />
                        <button type="submit">Sign Up!</button>
                     </fieldset>
                  </Form>
               )
            }}
         </Mutation>
      )
   }
}
