import React, { Component } from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 0.5fr 0.5fr;
  grid-gap: 5px;
  .buttonIncrementor {
    margin: 0 0 1rem 0;
  }

  input {
    border-radius: 10px;
  }
`;

const Restriction = styled.p`
  font-size: 1rem;
  color: ${props => props.inputColor};
  margin: 0 0 1rem 1rem;
`;

export default class Incrementor extends Component {
  constructor(props) {
    super(props);

    this.max = this.props.max || 1000;
    this.min = this.props.min || -100;

    this.state = {
      value: this.props.value,
      warning: false
    };
  }

  increment = async e => {
    e.preventDefault();
    if (this.state.value < this.max) {
      await this.setState(prevState => ({ value: ++prevState.value }));
      this.setState({ warning: false });
      this.props.onChange(this.state.value);
    }
  };

  decrement = async e => {
    e.preventDefault();
    if (this.state.value > this.min) {
      await this.setState(prevState => ({ value: --prevState.value }));
      this.setState({ warning: false });
      this.props.onChange(this.state.value);
    }
  };

  updateValue = async e => {
    let val = Number(e);
    if (Number.isInteger(val) && val >= this.min && val <= this.max) {
      this.setState({ warning: false });
      await this.setState({ value: val });
      this.props.onChange(this.state.value);
    } else {
      this.setState({ warning: true });
    }
  };

  render() {
    return (
      <Wrapper>
        <input
          type="text"
          value={this.state.value.toString()}
          onChange={e => this.updateValue(e.target.value)}
        />
        <button className="buttonIncrementor" onClick={e => this.decrement(e)}>&nbsp;-&nbsp;</button>
        <button className="buttonIncrementor" onClick={e => this.increment(e)}>+</button>
        {this.state.warning &&
          <Restriction inputColor="red">
            Must be between {this.min} to {this.max}
          </Restriction>
        }
      </Wrapper>
    );
  }
}
