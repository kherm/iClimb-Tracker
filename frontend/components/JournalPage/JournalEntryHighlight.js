import styled from "styled-components";
import Link from "next/link";
import { formatDate } from '../../lib/Date';
import { theme } from '../styles/GlobalStyles';

const CommentBox = styled.div`
  border-bottom: solid grey 1px;
  margin: 1rem 0;
  padding: 1rem;
  p {
    margin: 1rem 1.5rem;
    max-height: 72px;
    max-width: 95%;
    overflow: hidden;
    text-overflow: ellipsis;
    font-size: 1.2rem;
  }
`;

const DateLoc = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  margin: 1rem 1.5rem;
  .project {
    color: ${theme.blue};
  }
`;

const JournalEntryHighlight = props => {
  const { updatedAt, comment, project, id } = props.entry;

  return (
    <CommentBox>
      <Link href={{
        pathname: '/journalEntry',
        query: { id },
      }}>
        <a>
          <DateLoc>
            <label>{formatDate(updatedAt, "month", "day", "year")}</label>
            {project ? <label className="project">Project: {project.name}</label> : <></>}
          </DateLoc>
          <p>{comment}</p>
        </a>
      </Link>
    </CommentBox>
  );
};

export default JournalEntryHighlight;
export { DateLoc }
