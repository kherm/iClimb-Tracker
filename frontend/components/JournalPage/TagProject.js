import React, { Component } from "react";
import Downshift, { resetIdCounter } from 'downshift';
import { ApolloConsumer } from 'react-apollo';
import gql from 'graphql-tag';
import debounce from 'lodash.debounce';
import { DropDown, DropDownItem, SearchStyles } from '../styles/DropDown';

const SEARCH_PROJECTS_QUERY = gql`
  query SEARCH_PROJECTS_QUERY($searchTerm: String!) {
    projects(searchTerm: $searchTerm) {
      id
      name
    }
  }
`;

export default class TagProject extends Component {
   state = {
      projects: [],
      loading: false,
   };

   onChange = debounce(async (e, client) => {
      this.setState({ loading: true });
      const res = await client.query({
         query: SEARCH_PROJECTS_QUERY,
         variables: { searchTerm: e.target.value }
      });
      this.setState({
         projects: res.data.projects,
         loading: false,
      });
   }, 350);

   selectItem = (project) => {
      if (project !== null) {
         this.props.onTag(project.id);
      }
      else {
         this.props.onTag('')
      }
   }

   render() {
      resetIdCounter();
      return (
         <SearchStyles>
            <Downshift
               onChange={this.selectItem}
               itemToString={project => (project === null ? '' : project.name)}
            >
               {({ getInputProps, getItemProps, isOpen, inputValue, highlightedIndex, clearSelection }) => (
                  <div>
                     <ApolloConsumer>
                        {client => (
                           <input
                              {...getInputProps({
                                 type: "search",
                                 placeholder: "Tag a project",
                                 id: 'search',
                                 className: this.state.loading ? 'loading' : '',
                                 onChange: e => {
                                    e.persist();
                                    this.onChange(e, client);
                                 },
                              })}
                           />
                        )}
                     </ApolloConsumer>
                     {isOpen && (
                        <DropDown>
                           {this.state.projects.map((item, index) => (
                              <DropDownItem
                                 {...getItemProps({ item })}
                                 key={item.id}
                                 highlighted={index === highlightedIndex}
                              >
                                 <p>{item.name}</p>
                              </DropDownItem>
                           ))}
                           {!this.state.projects.length && !this.state.loading &&
                              <DropDownItem>Nothing found for "{inputValue}"</DropDownItem>}
                           {!inputValue && clearSelection()}
                        </DropDown>
                     )}
                  </div>
               )}
            </Downshift>
         </SearchStyles>
      );
   }
}
