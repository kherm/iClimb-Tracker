import React, { Component } from "react";
import Downshift, { resetIdCounter } from 'downshift';
import { ApolloConsumer } from 'react-apollo';
import gql from 'graphql-tag';
import debounce from 'lodash.debounce';
import { DropDown, DropDownItem, SearchStyles } from '../styles/DropDown';

const SEARCH_ENTRIES_QUERY = gql`
  query SEARCH_ENTRIES_QUERY($searchTerm: String!) {
    journalEntries(searchTerm: $searchTerm) {
      id
      comment
      updatedAt
      project {
        id
        name
      }
    }
  }
`;

export default class SearchBar extends Component {
  state = {
    journalEntries: [],
    loading: false,
  }

  onChange = debounce(async (e, client) => {
    this.setState({ loading: true });

    const res = await client.query({
      query: SEARCH_ENTRIES_QUERY,
      variables: { searchTerm: e.target.value }
    });

    this.setState({
      journalEntries: res.data.journalEntries,
      loading: false,
    });

    this.props.updateResults(this.state.journalEntries);
  }, 350);

  render() {
    return (
      <SearchStyles>
        <ApolloConsumer>
          {client => (
            <input
              type="search"
              placeholder="Search"
              className={this.state.loading ? 'loading' : ''}
              onChange={e => {
                e.persist();
                this.onChange(e, client)
              }}
            />
          )}
        </ApolloConsumer>
      </SearchStyles>
    );
  }
}
