import React, { Component } from 'react';
import { Query } from 'react-apollo';
import styled from 'styled-components';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';
import { Wrapper } from '../styles/GlobalStyles';
import SearchBar from './SearchBar';
import NewJournalEntry from "./NewJournalEntry";
import JournalEntryHighlight from './JournalEntryHighlight';

const LOAD_USER_JOURNAL_QUERY = gql`
  query LOAD_USER_JOURNAL_QUERY($searchTerm: String) {
    journalEntries(searchTerm: $searchTerm) {
      id
      comment
      updatedAt
      project {
        id
        name
      }
    }

  }
`;

const OList = styled.ol`
  list-style-type: none;
  padding: 0;
`;

class JournalList extends Component {
  state = {
    searchEntries: []
  }

  render() {
    const { searchEntries } = this.state;
    return (
      <Query query={LOAD_USER_JOURNAL_QUERY} variables={{ searchTerm: '' }}>
        {({ data, error, loading, refetch }) => {
          if (loading) return <p>Loading...</p>;
          if (error) return <p>Error: {error.message}</p>;
          return (
            <Wrapper>
              <SearchBar updateResults={res => this.setState({ searchEntries: res })} />
              {(!searchEntries.length || searchEntries.length === data.journalEntries.length)
                && <NewJournalEntry onSubmit={e => refetch()} />}
              <Entries list={!searchEntries.length && data.journalEntries || searchEntries} />
            </Wrapper>
          );
        }}
      </Query>
    );
  }
}

const Entries = props => {
  if (!props.list.length) {
    return <p>No entries yet.</p>
  }
  return (
    <OList>
      {props.list.map(entry =>
        <JournalEntryHighlight entry={entry} key={entry.id} />
      )}
    </OList>
  );
}

Entries.propTypes = {
  list: PropTypes.array,
};

export default JournalList;
export { LOAD_USER_JOURNAL_QUERY }