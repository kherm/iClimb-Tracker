import React, { Component } from 'react'
import styled from 'styled-components';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import AddButton from '../AddButton';
import TagProject from './TagProject';

const Textarea = styled.textarea`
   font-family: Verdana, Geneva, Tahoma, sans-serif;
   font-size: inherit;
`;

const Buttons = styled.div`
   display: flex;
   justify-content: flex-end;
   align-items: center;

   input {
      height: 50%;
      padding: 1rem;
   }
   button {
      width: 33%;
      margin-left: 1rem;
   }
   .cancel {
      background-color: rgba(255, 0, 0, 0.75);
   }
`;

const CREATE_JOURNALENTRY_MUTATION = gql`
   mutation CREATE_JOURNALENTRY_MUTATION($projectId: ID, $comment: String!) {
      createJournalEntry(projectId: $projectId, comment: $comment) {
         id
         comment
         updatedAt
         project {
            id
            name
         }
      }
   }
`;

class NewJournalEntry extends Component {
   state = {
      projectId: "",
      comment: ""
   }

   render() {
      return (
         <Mutation
            mutation={CREATE_JOURNALENTRY_MUTATION}
            variables={{ projectId: this.state.projectId, comment: this.state.comment }}
            update={e => this.props.onSubmit()}
         >
            {(createJournalEntry) => (
               <form onSubmit={e => {
                  e.preventDefault();
                  if (this.state.comment === "") return;
                  createJournalEntry();
                  this.setState({ comment: "" });
               }}>
                  <Textarea
                     rows="5"
                     name="comment"
                     placeholder=" What's happening today?"
                     value={this.state.comment}
                     onChange={e => this.setState({ comment: e.target.value })}
                  >
                  </Textarea>
                  <Buttons>
                     <TagProject onTag={name => this.setState({ projectId: name })} />
                     <AddButton
                        label="Save"
                        fontSize="1.5rem"
                        type="submit"
                     />
                  </Buttons>
               </form>
            )}
         </Mutation>
      );
   }
}

export default NewJournalEntry;
export { Textarea, Buttons }