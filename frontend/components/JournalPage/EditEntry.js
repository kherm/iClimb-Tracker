import React, { Component } from 'react'
import { Query, Mutation } from 'react-apollo';
import Router from 'next/router';
import gql from 'graphql-tag';
import { Wrapper } from '../styles/GlobalStyles';
import { Textarea, Buttons } from './NewJournalEntry';
import { DateLoc } from './JournalEntryHighlight';
import AddButton from '../AddButton';
import { saveNotification } from '../../lib/Notification';
import { LOAD_USER_JOURNAL_QUERY } from './JournalList';

const SINGLE_JOURNALENTRY_QUERY = gql`
   query SINGLE_JOURNALENTRY_QUERY($id: ID!) {
      journalEntry(where: {id: $id}) {
         id
         comment
         project {
            id
            name
         }
         createdAt
         updatedAt
      }
   }
`;

const UPDATE_JOURNALENTRY_MUTATION = gql`
   mutation UPDATE_JOURNALENTRY_MUTATION($id: ID!, $comment: String!) {
      updateJournalEntry(id: $id, comment: $comment) {
         id
      }
   }
`;

const EditEntry = (props) => (
   <Query query={SINGLE_JOURNALENTRY_QUERY} variables={{ id: props.id }}>
      {({ data }, loading, error) => {
         if (loading) return <p>Loading...</p>;
         return <EditForm data={data.journalEntry} />
      }}
   </Query>
);


class EditForm extends Component {
   state = {
      comment: this.props.data.comment
   }

   render() {
      const { id, updatedAt, project } = this.props.data
      const date = new Date(updatedAt);
      const dateStr = `${date.getMonth() + 1}-${date.getDate()}-${date.getFullYear()}`;

      return (
         <Mutation
            mutation={UPDATE_JOURNALENTRY_MUTATION}
            variables={{ id, comment: this.state.comment }}
            refetchQueries={[{ query: LOAD_USER_JOURNAL_QUERY }]}
         >
            {(updateJournalEntry) => (
               <Wrapper>
                  <DateLoc>
                     <label>Last updated: {dateStr}</label>
                     {project ? <label>Project: {project.name}</label> : <></>}
                  </DateLoc>
                  <form onSubmit={e => {
                     e.preventDefault();
                     updateJournalEntry();
                     saveNotification('Journal Entry Updated!');
                     Router.push('/journal')
                  }}>
                     <Textarea
                        rows="5"
                        name="comment"
                        value={this.state.comment}
                        onChange={e => this.setState({ comment: e.target.value })}
                     >
                     </Textarea>
                     <Buttons>
                        <AddButton
                           label="Cancel"
                           className="cancel"
                           fontSize="1.5rem"
                           onClick={e => Router.push('/journal')}
                        />
                        <AddButton
                           label="Save"
                           fontSize="1.5rem"
                           type="submit"
                        />
                     </Buttons>
                  </form>
               </Wrapper>
            )}
         </Mutation>
      );
   }
};

export default EditEntry;