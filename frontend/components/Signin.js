import React, { Component } from 'react'
import Router from 'next/router';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import styled from 'styled-components';
import { theme } from './styles/GlobalStyles';
import Form from './styles/Form'
import DisplayError from './DisplayError';
import * as queries from './StatsPage/StatsContainer';
import { LOAD_USER_JOURNAL_QUERY } from './JournalPage/JournalList';
import { LOAD_USER_PROJECTS_QUERY } from './ProjectsPage/RouteList';
import { CURRENT_USER_QUERY } from './User';
import Toggleable from './Toggleable';
import RequestReset from './RequestReset';


const SIGNIN_MUTATION = gql`
   mutation SIGNIN_MUTATION($email: String!, $password: String!) {
      signin(email: $email, password: $password) {
         id
         name
      }
   }
`;

const P = styled.p`
   text-align: center;
   cursor: pointer;
   color: ${theme.blue};
`;

export default class Signin extends Component {
   state = {
      email: '',
      password: '',
   }

   saveToState = (e) => {
      this.setState({ [e.target.name]: e.target.value });
   }

   render() {
      return (
         <Mutation
            mutation={SIGNIN_MUTATION}
            variables={this.state}
            refetchQueries={[
               { query: queries.USER_HIGHEST_GRADE_QUERY },
               { query: queries.USER_PITCHES_MONTH_QUERY },
               { query: queries.USER_TOT_DAYS_YEAR_QUERY },
               { query: queries.USER_TOT_VERTICAL_QUERY },
               { query: queries.GRADES_VS_COUNTS_QUERY },
               { query: queries.STYLE_VS_TIME_QUERY },
               { query: CURRENT_USER_QUERY },
               { query: LOAD_USER_JOURNAL_QUERY },
               { query: LOAD_USER_PROJECTS_QUERY },
            ]}
            awaitRefetchQueries={true}
         >
            {(signin, { error, loading }) => {
               return (
                  <Form method="post" onSubmit={async e => {
                     e.preventDefault();
                     await signin();
                     await this.props.onLogin();
                     window.location.reload();
                     Router.replace('/stats');
                  }}>
                     <fieldset disabled={loading} aria-busy={loading}>
                        <h2>Login to iClimb</h2>
                        <DisplayError error={error} />
                        <input
                           type="text"
                           name="email"
                           placeholder="Email"
                           value={this.state.email}
                           onChange={this.saveToState}
                           required
                        />
                        <input
                           type="password"
                           name="password"
                           placeholder="Password"
                           value={this.state.password}
                           onChange={this.saveToState}
                           required
                        />
                        <button type="submit">Login</button>
                        <Toggleable>
                           {(show, toggle) => (
                              <>
                                 <P onClick={toggle}>Forgot your password?</P>
                                 {show ? <RequestReset /> : <></>}
                              </>
                           )}
                        </Toggleable>
                     </fieldset>
                  </Form>
               )
            }}
         </Mutation>
      )
   }
}