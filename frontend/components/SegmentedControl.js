import React, { Component } from "react";
import styled from "styled-components";
import { theme } from './styles/GlobalStyles';

const Buttons = styled.div`
  display: flex;
  * {
    flex-grow: 1;
  }

  input {
    cursor: pointer;
    border: 1px solid grey;
    border-radius: 10px;
    box-shadow: 1px 1px grey;
  }
  .selectedBtn {
    background: ${theme.green};
    color: ${theme.yellow};
  }

  .unselectedBtn {
    background: white;
  }
`;

export default class SegmentedControl extends Component {
  constructor(props) {
    super(props);

    const { options, preSelect } = this.props;
    this.types = Array(options.length).fill(false);
    if (preSelect) {
      const preSelectIndex = options.indexOf(preSelect);
      this.types[preSelectIndex] = true;
    }

    this.state = {
      toggle: this.types
    };
  }

  handleClick = (e, i) => {
    this.props.onSelect(e.target.value);
    let newState = this.state.toggle.fill(false);
    newState[i] = !newState[i];
    this.setState({ toggle: newState });
  };

  render() {
    return (
      <Buttons>
        {this.props.options.map((type, i) => (
          <input
            className={this.state.toggle[i] ? "selectedBtn" : "unselectedBtn"}
            type="button"
            value={type}
            key={type}
            onClick={e => this.handleClick(e, i)}
          />
        ))}
      </Buttons>
    );
  }
}
