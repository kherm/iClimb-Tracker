import React, { Component } from 'react'
import styled from 'styled-components';

const ErrorStyles = styled.div`
   color: red;
   text-align: center;
`;

let checkError = (error) => {
   if (error.includes('email') && error.includes('unique'))
      return 'This email is already in our system.';
   return error.replace('GraphQL error: ', '');
}

const DisplayError = ({ error }) => {
   if (!error) return null;
   let message = checkError(error.message);
   return (
      <ErrorStyles>
         {message}
      </ErrorStyles>
   );
}

export default DisplayError;