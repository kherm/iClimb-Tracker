import React, { Component } from 'react'
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import styled from 'styled-components';
import Form from './styles/Form'
import { theme } from './styles/GlobalStyles';
import DisplayError from './DisplayError';

const REQUEST_RESET_MUTATION = gql`
   mutation REQUEST_RESET_MUTATION($email: String!) {
      requestReset(email: $email) {
         message
      }
   }
`;

const Note = styled.p`
   color: ${theme.blue};
   margin: 0.2rem;
   text-align: center;
`;

export default class RequestReset extends Component {
   state = {
      email: '',
   }

   saveToState = (e) => {
      this.setState({ [e.target.name]: e.target.value });
   }

   render() {
      return (
         <Mutation
            mutation={REQUEST_RESET_MUTATION}
            variables={this.state}
         >
            {(reset, { error, loading, called }) => {
               return (
                  <Form method="post" onSubmit={async e => {
                     e.preventDefault();
                     e.stopPropagation();
                     await reset();
                  }}>
                     <fieldset disabled={loading} aria-busy={loading}>
                        <h2>Request a password reset</h2>
                        <DisplayError error={error} />
                        {loading && called && <Note>Sending email...</Note>}
                        {!error && !loading && called &&
                           <>
                              <Note>Success! Check your email for a reset link.</Note>
                              <Note>It might be in your spam folder.</Note>
                           </>
                        }
                        <input
                           type="text"
                           name="email"
                           placeholder="Email"
                           value={this.state.email}
                           onChange={this.saveToState}
                           required
                        />
                        <button type="submit">Request Reset</button>
                     </fieldset>
                  </Form>
               )
            }}
         </Mutation>
      )
   }
}