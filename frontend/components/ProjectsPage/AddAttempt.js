import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import Form from "../styles/Form"
import DisplayError from "../DisplayError";
import Incrementor from "../Incrementor";
import SegmentedControl from "../SegmentedControl";
import { sendTypes } from "../../lib/ClimbData";
import { LOAD_USER_PROJECTS_QUERY } from './RouteList';
import { LOAD_USER_JOURNAL_QUERY } from '../JournalPage/JournalList';
import { SINGLE_PROJECT_QUERY } from './SingleProject';
import { sendAttemptQuestion, noChangesMadeNotification, saveNotification } from '../../lib/Notification';


const CREATE_ATTEMPT_MUTATION = gql`
  mutation CREATE_ATTEMPT_MUTATION(
    $projectId: ID!,
    $falls: Int!,
    $takes: Int!,
    $sendType: String
  ) {
    createAttempt(
      projectId: $projectId,
      falls: $falls,
      takes: $takes,
      sendType: $sendType
    ) {
      id
    }
  }
`;

const CREATE_JOURNALENTRYFORATTEMPT_MUTATION = gql`
  mutation CREATE_JOURNALENTRYFORATTEMPT_MUTATION(
    $attemptId: ID!,
    $projectId: ID!,
    $comment: String!
  ) {
    createJournalEntryForAttempt(
      attemptId: $attemptId,
      projectId: $projectId,
      comment: $comment
    ) {
      id
    }
  }
`;

export default class AddAttempt extends Component {
   state = {
      projectId: this.props.id,
      attemptId: "",
      falls: 0,
      takes: 0,
      sendType: "",
      comment: "",
   }

   handleSubmit = async (e, mutations) => {
      e.preventDefault();
      let send = false;
      if (this.state.falls == 0 && this.state.takes == 0) {
         if (this.state.sendType === "Redpoint") {
            send = await sendAttemptQuestion();
         }
         else if (this.state.sendType === "" && this.state.comment === "") {
            noChangesMadeNotification();
            return
         }
      }
      if (!send) {
         const attempt = await mutations[0]();
         await this.setState({ attemptId: attempt.data.createAttempt.id });
         if(this.state.comment !== "" ) {
            mutations[1]();
         }

         saveNotification("Attempt added!")
      }
      this.props.onCancel();
   }

   render() {
      return (
         <Mutation
            mutation={CREATE_ATTEMPT_MUTATION}
            variables={this.state}
            refetchQueries={[
               { query: LOAD_USER_PROJECTS_QUERY },
               { query: SINGLE_PROJECT_QUERY, variables: { id: this.props.id } }
            ]}
            awaitRefetchQueries={true}
         >
         {(createAttempt, { loading, error }) => (

            <Mutation
               mutation={CREATE_JOURNALENTRYFORATTEMPT_MUTATION}
               variables={{
                  attemptId: this.state.attemptId,
                  projectId: this.state.projectId,
                  comment: this.state.comment
               }}
               refetchQueries={[{query: LOAD_USER_JOURNAL_QUERY, variables:{ searchTerm: '' }}]}
            >
            {(createJournalEntryForAttempt) => {
               if (loading) return <p>Loading...</p>
               return (
               <Form
                  method="post"
                  onSubmit={ e => this.handleSubmit(e, [createAttempt, createJournalEntryForAttempt])}
                  addClimb
               >
                  <fieldset>
                     <h2>Add an Attempt:</h2>
                     <DisplayError>{error}</DisplayError>

                     <span>Falls:</span>
                     <Incrementor
                        value={this.state.falls}
                        max="50"
                        min="0"
                        onChange={e => this.setState({ falls: e })}
                        />

                     <span>Takes:</span>
                     <Incrementor
                        value={this.state.takes}
                        max="50"
                        min="0"
                        onChange={e => this.setState({ takes: e })}
                        />

                     <span>Send:</span>
                     <SegmentedControl
                        options={sendTypes.filter(type => type !== "Onsight")}
                        onSelect={selectedOption =>
                           this.setState({ sendType: selectedOption })
                        }
                        />

                     <span>Comments:</span>
                     <textarea
                        rows="5"
                        name="comment"
                        placeholder=" How's your stoke?
                        What did you do well or can improve on?
                        Comments will be saved to your journal."
                        onChange={e => this.setState({ comment: e.target.value })}
                        >
                     </textarea>

                     <div className="buttons">
                        <button
                           className="cancel"
                           type="button"
                           onClick={e => this.props.onCancel()}
                        >Cancel</button>
                        <button type="submit">Add</button>
                     </div>
                  </fieldset>
               </Form>
            )}}
            </Mutation>
      )}
      </Mutation>
      )
   }
}

export {
   CREATE_ATTEMPT_MUTATION,
   CREATE_JOURNALENTRYFORATTEMPT_MUTATION,
}
