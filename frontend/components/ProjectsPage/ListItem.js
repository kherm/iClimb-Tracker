import React from "react";
import PropTypes from 'prop-types';
import Link from 'next/link';
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import { sendQuestion } from '../../lib/Notification';
import { List, ItemStyle } from '../styles/ListStyles';
import { LOAD_USER_PROJECTS_QUERY } from './RouteList';

const COMPLETE_PROJECT_MUTATION = gql`
   mutation COMPLETE_PROJECT_MUTATION(
      $id: ID!,
      $completedDate: DateTime
   ) {
      updateProjectCompletedDate(
         id: $id,
         completedDate: $completedDate
      ) {
         id
      }
   }
`;

const ListItem = ({ data }) => {
   const today = new Date();
   const todayStr = `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`;
   let complete = "";
   if (data.completedDate !== null) {
      complete = "completed"
   }

   const checkComplete = async (e, mutation) => {
      if (e.target.checked && data.completedDate === null) {
         e.target.checked = false;
         const send = await sendQuestion();
         if (send) { mutation() }
      } else if (data.completedDate !== null) {
         e.target.checked = true;
      }
   }

   return (
      <Mutation
         mutation={COMPLETE_PROJECT_MUTATION}
         variables={{ id: data.id, completedDate: todayStr }}
         refetchQueries={[{ query: LOAD_USER_PROJECTS_QUERY }]}
         awaitRefetchQueries={true}
      >
         {(updateProjectCompletedDate) =>
            <List>
               <ItemStyle className={complete}>
                  <Link href={{
                     pathname: '/project',
                     query: { id: data.id },
                  }}
                  >
                     <a>
                        <div>
                           <p className="name">{data.name}</p>
                           <p className="detail">Attempts: {data.attempts.length}</p>
                        </div>
                        <p className="gradeCol">{data.grade}</p>
                     </a>
                  </Link>
                  <input
                     type="checkbox"
                     onChange={e => { checkComplete(e, updateProjectCompletedDate) }}
                     defaultChecked={data.completedDate !== null}
                  />
               </ItemStyle>
            </List>
         }
      </Mutation>
   );
}

ListItem.propTypes = {
   name: PropTypes.string,
   grade: PropTypes.string,
};

export default ListItem;
export { COMPLETE_PROJECT_MUTATION };