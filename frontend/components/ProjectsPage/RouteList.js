import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import router from 'next/router';
import { Wrapper } from '../styles/GlobalStyles';
import List from "./List";
import AddButton from "../AddButton";
import User from "../User";

const LOAD_USER_PROJECTS_QUERY = gql`
  query LOAD_USER_PROJECTS_QUERY {
    projects(orderBy: completedDate_ASC) {
      id
      name
      grade
      completedDate
      attempts {
         id
      }
    }
  }
`;

const RouteList = (props) => (
   <User>
      {({ data: { me } }) => (
         me && (<Query query={LOAD_USER_PROJECTS_QUERY}>
            {({ data, error, loading }) => {
               if (loading) return <p>Loading...</p>;
               if (error) return <p>Error: {error.message}</p>;
               return (
                  <Wrapper>
                     <AddButton label="+ Project" onClick={e => router.push('/addProject')} />
                     <List
                        title={`${me.name}'s Projects`}
                        data={data.projects.filter(project => project.completedDate === null)}
                        emptyNote="None yet... time to get inspired!"
                     />
                     {data.projects.filter(project => project.completedDate !== null).length !== 0 ?
                        <List
                           title={"The Archives"}
                           data={data.projects.filter(project => project.completedDate !== null)}
                           completed
                        /> : <></>
                     }
                  </Wrapper>
               );
            }}
         </Query>
         ))}
   </User>
);

export default RouteList;
export { LOAD_USER_PROJECTS_QUERY };
