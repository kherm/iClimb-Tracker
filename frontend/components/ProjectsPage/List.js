import React from 'react';
import PropTypes from 'prop-types';
import { ListStyle } from '../styles/ListStyles';
import ListItem from './ListItem';


const List = (props) => (
   <ListStyle>
      <h3>{props.title}</h3>
      {props.data.length === 0 ? <p className="note">{props.emptyNote}</p> : ""}
      <ol className={props.completed ? "completed" : ""}>
         {props.data.map(item =>
            <ListItem data={item} key={item.id} />
         )}
      </ol>
   </ListStyle >
);

List.propTypes = {
   title: PropTypes.string,
   data: PropTypes.array,
   emptyNote: PropTypes.string,
}

export default List;