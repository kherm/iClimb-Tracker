import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import Form from "../styles/Form";
import DisplayError from "../DisplayError";
import Select from "../Select";
import Incrementor from "../Incrementor";
import SegmentedControl from "../SegmentedControl";
import CheckboxGroup from "../CheckboxGroup";
import { grades, climbStyle, routeStyles } from "../../lib/ClimbData";
import { LOAD_USER_PROJECTS_QUERY } from './RouteList';
import { SINGLE_PROJECT_QUERY } from './SingleProject';
import { saveNotification } from '../../lib/Notification';
import DeleteProject from '../DeleteProject';

const UPDATE_PROJECT_MUTATION = gql`
  mutation UPDATE_PROJECT_MUTATION(
    $id: ID!,
    $name: String,
    $grade: String,
    $pitches: Int,
    $totalLength: Int,
    $climbStyle: String,
    $routeStyles: [RouteStyle]!,
  ) {
    updateProject(
      id: $id,
      name: $name,
      grade: $grade,
      pitches: $pitches,
      totalLength: $totalLength,
      climbStyle: $climbStyle,
      routeStyles: $routeStyles,
    ) {
      id
    }
  }
`;

export default class EditProject extends Component {
   constructor(props) {
      super(props);
      const { id, name, grade, pitches, totalLength, climbStyle, routeStyles } = this.props.data;

      this.state = {
         id,
         name,
         grade,
         pitches,
         totalLength,
         climbStyle,
         routeStyles,
      }
   }

   render() {
      return (
         <Mutation
            mutation={UPDATE_PROJECT_MUTATION}
            variables={this.state}
            refetchQueries={[
               { query: LOAD_USER_PROJECTS_QUERY },
               { query: SINGLE_PROJECT_QUERY, variables: { id: this.state.id } },
            ]}
            awaitRefetchQueries={true}
         >
            {(updateProject) =>

               <Form
                  method="post"
                  onSubmit={e => {
                     e.preventDefault();
                     updateProject();
                     saveNotification('Project Saved!');
                     this.props.onCancel();
                  }}
                  addClimb
               >
                  <fieldset>
                     <h2>Edit Project - {this.state.name}</h2>
                     <DisplayError></DisplayError>

                     <span>Name:</span>
                     <input
                        type="text"
                        value={this.state.name}
                        onChange={e => this.setState({ name: e.target.value })}
                     />

                     <span>Grade:</span>
                     <Select
                        options={grades}
                        preSelect={this.state.grade}
                        onSelect={selectedOption =>
                           this.setState({ grade: selectedOption })
                        }
                     />

                     <span>Pitches:</span>
                     <Incrementor
                        value={this.state.pitches}
                        max="50"
                        min="0"
                        onChange={e => this.setState({ pitches: e })}
                     />

                     <span>Total Length:</span>
                     <Incrementor
                        value={this.state.totalLength}
                        min="0"
                        onChange={e => this.setState({ totalLength: e })}
                     />

                     <span>Style:</span>
                     <SegmentedControl
                        options={climbStyle}
                        preSelect={this.state.climbStyle}
                        onSelect={selectedOption =>
                           this.setState({ climbStyle: selectedOption })
                        }
                     />

                     <span>Route Styles:</span>
                     <CheckboxGroup
                        options={routeStyles}
                        preSelect={this.state.routeStyles}
                        onChange={e => this.setState({ routeStyles: e })}
                     />

                     <div className="buttons">
                        <button
                           className="cancel"
                           type="button"
                           onClick={e => this.props.onCancel()}
                        >Cancel</button>
                        <button type="submit">Save</button>
                     </div>
                     <div className="buttons">
                        <DeleteProject id={this.state.id} />
                     </div>
                  </fieldset>
               </Form>
            }
         </Mutation>
      )
   }
}

export { UPDATE_PROJECT_MUTATION };
