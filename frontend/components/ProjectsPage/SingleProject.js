import React, { Component } from 'react'
import gql from "graphql-tag";
import { Query } from "react-apollo";
import Router from 'next/router';
import styled from 'styled-components';
import { Wrapper } from '../styles/GlobalStyles';
import EditProject from './EditProject';
import AddAttempt from './AddAttempt';
import AddButton from '../AddButton';
import AttemptsChart from './AttemptsChart';
import Chart from '../StatsPage/Chart';

const SINGLE_PROJECT_QUERY = gql`
   query SINGLE_PROJECT_QUERY($id: ID!) {
      project(where: {id: $id}) {
         id
         name
         grade
         pitches
         totalLength
         climbStyle
         routeStyles
         attempts (orderBy: createdAt_ASC){
            id
            falls
            takes
            sendType
            createdAt
         }
      }
   }
`;

const Buttons = styled.div`
   display: grid;
   grid-template-columns: 1fr 1fr;
   grid-gap: 10px;
   margin: 1.5rem;
   .backButton {
      grid-column: 1 / -1;
   }
`;

export default class SingleProject extends Component {
   state = {
      editProject: false,
      addAttempt: false
   }

   render() {
      const id = this.props.id;
      return (
         <Query query={SINGLE_PROJECT_QUERY} variables={{ id }}>
            {({ data, loading }) => {
               if (loading) return <p>Loading...</p>;
               return (
                  <Wrapper>
                     {!this.state.editProject && !this.state.addAttempt ?
                        <Buttons>
                           <AddButton
                              className="backButton"
                              label="Back to Project List"
                              fontSize="1.5rem"
                              backgroundColor="grey"
                              onClick={e => Router.push('/projects')}
                           />
                           <AddButton
                              label="Edit Project"
                              onClick={e => this.setState({ editProject: true, addAttempt: false })}
                           />
                           <AddButton
                              label="+ Attempt"
                              onClick={e => this.setState({ editProject: false, addAttempt: true })}
                           />
                        </Buttons>
                        : <></>
                     }
                     {this.state.editProject ?
                        <EditProject
                           data={data.project}
                           onCancel={e => this.setState({ editProject: false })}
                        /> : <></>
                     }
                     {this.state.addAttempt ?
                        <AddAttempt
                           id={id}
                           onCancel={e => this.setState({ addAttempt: false })}
                        /> : <></>
                     }
                     {!this.state.addAttempt && !this.state.editProject ?
                        <Chart
                           title={`${data.project.name} - Attempts over Time`}
                           maxPage={1}
                           hideButtons
                        >
                           {page => <AttemptsChart data={data.project.attempts} />}
                        </Chart> : <></>
                     }
                  </Wrapper>
               );
            }}
         </Query>
      )
   }
}

export { SINGLE_PROJECT_QUERY };