import React from 'react';
import {
   VictoryBar,
   VictoryStack,
   VictoryChart,
   VictoryLegend,
   VictoryAxis
} from 'victory';
import { theme } from '../styles/GlobalStyles';
import { Label, Container } from '../styles/ChartStyles';
import { formatDate } from '../../lib/Date';

const AttemptsChart = (props) => {
   const data = props.data;
   data.forEach(attempt => attempt.date = new Date(attempt.createdAt))
   //set x axis ticks to be middle of each month
   const dateCategories = data.map(attempt =>
      new Date(attempt.date.getFullYear(), attempt.date.getMonth(), 15))
      .filter((date, i, arr) => {
         if (i > 0) {
            return date.getMonth() !== arr[i - 1].getMonth() &&
               date.getFullYear() !== arr[i - 1].getFullYear()
         }
         return true;
      });
   if (dateCategories.length === 1) {
      dateCategories.push(
         new Date(dateCategories[0].getFullYear(), dateCategories[0].getMonth() + 1, 15)
      )
   }

   return (
      <Container>
         {data.length ? <></> : <Label>No climbs logged</Label>}
         <VictoryChart
            domainPadding={40}
            padding={{ top: 15, bottom: 50, right: 50, left: 50 }}
            scale={{ x: "time" }}
         >
            <VictoryAxis
               tickValues={dateCategories}
               label="Attempt"
               tickFormat={(t) => data.length ?
                  formatDate(t, "month").substr(0, 3) : ""
               }
            />
            <VictoryAxis
               dependentAxis
               label="Number of Falls / Takes"
               tickFormat={(t) => Math.round(t) === t ? t : undefined}
            />
            <VictoryLegend
               x={150} y={0}
               orientation="horizontal"
               gutter={20}
               colorScale={[`${theme.red}`, `#F96372`]}
               data={[{ name: "Falls" }, { name: "Takes" }]}
            />
            <VictoryStack domain={data.length < 5 ? { y: [0, 5] } : null}>
               <VictoryBar
                  style={{ data: { fill: `${theme.red}` } }}
                  data={data}
                  x="date"
                  y="falls"
                  barWidth={10}
                  animate={{
                     duration: 500,
                     onLoad: { duration: 300 }
                  }}
               />
               <VictoryBar
                  style={{ data: { fill: `#F96372` } }}
                  data={data}
                  x="date"
                  y="takes"
                  barWidth={10}
                  animate={{
                     duration: 500,
                     onLoad: { duration: 300 }
                  }}
               />
            </VictoryStack>
         </VictoryChart>
      </Container>
   )
};

export default AttemptsChart;