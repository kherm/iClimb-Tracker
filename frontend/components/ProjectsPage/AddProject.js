import React, { Component } from 'react'
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import Form from "../styles/Form";
import Select from "../Select";
import Incrementor from "../Incrementor";
import SegmentedControl from "../SegmentedControl";
import CheckboxGroup from "../CheckboxGroup";
import { grades, climbStyle, routeStyles } from "../../lib/ClimbData";
import DisplayError from '../DisplayError';
import { saveNotification } from '../../lib/Notification';
import { LOAD_USER_PROJECTS_QUERY } from './RouteList';

const CREATE_PROJECT_MUTATION = gql`
  mutation CREATE_PROJECT_MUTATION(
    $name: String!,
    $grade: String!,
    $pitches: Int!,
    $totalLength: Int!,
    $climbStyle: String,
    $routeStyles: [RouteStyle!]!,
  ) {
    createProject(
      name: $name,
      grade: $grade,
      pitches: $pitches,
      totalLength: $totalLength,
      climbStyle: $climbStyle,
      routeStyles: $routeStyles
    ) {
      id
      name
      grade
      climbStyle
      totalLength
    }
  }
`;

class AddProject extends Component {

   state = {
      name: "",
      grade: grades[0],
      pitches: 1,
      totalLength: 25,
      climbStyle: "",
      routeStyles: [],
   };

   handleSubmit = async (e, mutation) => {
      e.preventDefault();
      await mutation();
      saveNotification("Project Added!");
      this.props.onSubmit();
   }

   render() {
      return (
         <Mutation
            mutation={CREATE_PROJECT_MUTATION}
            variables={this.state}
            refetchQueries={[{ query: LOAD_USER_PROJECTS_QUERY }]}
            awaitRefetchQueries={true}
         >
            {(createProject, { loading, error }) => (
               <Form method="post"
                  onSubmit={e => this.handleSubmit(e, createProject)}
                  addClimb
               >
                  <fieldset disabled={loading} aria-busy={loading}>
                     <h2>Add Project</h2>
                     <DisplayError error={error} />
                     <span>Name:</span>
                     <input
                        type="text"
                        value={this.state.name}
                        onChange={e => this.setState({ name: e.target.value })}
                        autoFocus
                        required
                     />

                     <span>Grade:</span>
                     <Select
                        options={grades}
                        preSelect={this.state.grade}
                        onSelect={selectedOption =>
                           this.setState({ grade: selectedOption })
                        }
                     />

                     <span>Pitches:</span>
                     <Incrementor
                        value={this.state.pitches}
                        max="50"
                        min="0"
                        onChange={e => this.setState({ pitches: e })}
                     />

                     <span>Total Length of Pitches (m):</span>
                     <Incrementor
                        value={this.state.totalLength}
                        min="0"
                        onChange={e => this.setState({ totalLength: e })}
                     />

                     <span>Style:</span>
                     <SegmentedControl
                        options={climbStyle}
                        onSelect={selectedOption =>
                           this.setState({ climbStyle: selectedOption })
                        }
                     />

                     <span>Route Styles:</span>
                     <CheckboxGroup
                        options={routeStyles}
                        onChange={e => this.setState({ routeStyles: e })}
                     />

                     <div className="buttons">
                        <button
                           className="cancel"
                           type="button"
                           onClick={e => this.props.onSubmit()}
                        >Cancel</button>
                        <button type="submit">Save</button>
                     </div>
                  </fieldset>
               </Form>
            )}
         </Mutation>
      )
   }
}

export default AddProject;
export { CREATE_PROJECT_MUTATION };