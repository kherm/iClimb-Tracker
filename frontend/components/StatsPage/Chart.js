import React, { Component } from 'react';
import styled from "styled-components";
import PropTypes from 'prop-types';

const Box = styled.div`
  border: solid black 1px;
  position: relative;
`;

const Title = styled.h3`
  text-align: center;
  margin-bottom: 0;
`;

const Buttons = styled.div`
  display: ${props => props.hide ? "none" : "flex"};
  position: absolute;
  top: 40%;
  z-index: 2;
  justify-content: space-between;
  button {
    width: 40px;
    height: 35px;
    font-weight: bolder;
    border: 1px solid lightgrey;
    opacity: 0.5;
    &:hover {
      opacity: 1;
    }
  }
`;

class Chart extends Component {
  state = {
    page: 0,
  }

  prevPage = () => {
    if (this.state.page > 0) {
      this.setState({ page: this.state.page - 1 })
    }
  }

  nextPage = () => {
    const MAX_PAGES = this.props.maxPage - 1;
    if (this.state.page < MAX_PAGES) {
      this.setState({ page: this.state.page + 1 })
    }
  }

  render() {
    const page = this.state.page;
    return (
      <Box>
        <Title>{this.props.title}</Title>
        <Buttons hide={this.props.hideButtons ? "true" : ""}>
          <button onClick={this.prevPage}>&lt;</button>
          <button onClick={this.nextPage}>&gt;</button>
        </Buttons>
        {this.props.children(page)}
      </Box>
    );
  };
};

Chart.propTypes = {
  children: PropTypes.func.isRequired,
}

export default Chart;
