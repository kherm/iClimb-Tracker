import React, { Component } from 'react'
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import Form from "../styles/Form";
import Select from "../Select";
import Incrementor from "../Incrementor";
import SegmentedControl from "../SegmentedControl";
import CheckboxGroup from "../CheckboxGroup";
import { grades, climbStyle, routeStyles, sendTypes } from "../../lib/ClimbData";
import DisplayError from '../DisplayError';
import * as queries from './StatsContainer';
import { saveNotification } from '../../lib/Notification';

const CREATE_CLIMB_MUTATION = gql`
  mutation CREATE_CLIMB_MUTATION(
    $grade: String!,
    $pitches: Int!,
    $totalLength: Int!,
    $sendType: String,
    $climbStyle: String,
    $routeStyles: [RouteStyle!]!,
  ) {
    createClimb(
      grade: $grade,
      pitches: $pitches,
      totalLength: $totalLength,
      sendType: $sendType,
      climbStyle: $climbStyle,
      routeStyles: $routeStyles
    ) {
      id
      grade
      pitches
      totalLength
      createdAt
    }
  }
`;

const CREATE_JOURNALENTRYFORCLIMB_MUTATION = gql`
  mutation CREATE_JOURNALENTRYFORCLIMB_MUTATION(
    $climbId: ID!,
    $comment: String!
  ) {
    createJournalEntryForClimb(
      climbId: $climbId,
      comment: $comment
    ) {
      id
    }
  }
`;

class AddClimb extends Component {

   state = {
      grade: grades[0],
      pitches: 1,
      totalLength: 25,
      sendType: "",
      climbStyle: "",
      routeStyles: [],
      climbId: "",
      comment: "",
   };

   handleSubmit = async (e, mutations) => {
      e.preventDefault();
      const res = await mutations[0]();
      await this.setState({climbId: res.data.createClimb.id});

      if (this.state.comment !== "") {
         await mutations[1]();
      }
      saveNotification('Climb added!');
      this.props.onSubmit();
   }

   render() {
      return (
         <Mutation
            mutation={CREATE_CLIMB_MUTATION}
            variables={this.state}
            refetchQueries={[
               {query: queries.USER_HIGHEST_GRADE_QUERY},
               {query: queries.USER_PITCHES_MONTH_QUERY},
               {query: queries.USER_TOT_DAYS_YEAR_QUERY},
               {query: queries.USER_TOT_VERTICAL_QUERY},
               {query: queries.GRADES_VS_COUNTS_QUERY},
               {query: queries.STYLE_VS_TIME_QUERY},
            ]}
            awaitRefetchQueries={true}
         >
            {(createClimb, { loading, error }) => (

               <Mutation
                  mutation={CREATE_JOURNALENTRYFORCLIMB_MUTATION}
                  variables={{climbId: this.state.climbId, comment: this.state.comment}}
               >
                  {(createJournalEntry) => (

                  <Form
                     method="post"
                     onSubmit={e => this.handleSubmit(e, [createClimb, createJournalEntry])}
                     addClimb
                  >
                     <fieldset disabled={loading} aria-busy={loading}>
                        <h2>Add Climb</h2>
                        <DisplayError error={error} />
                        <span className="note">For multipitch climbs, enter separately the number of pitches for each grade.</span>
                        <span>Grade:</span>
                        <Select
                           options={grades}
                           preSelect={this.state.grade}
                           onSelect={selectedOption =>
                              this.setState({ grade: selectedOption })
                           }
                           />

                        <span>Pitches:</span>
                        <Incrementor
                           value={this.state.pitches}
                           max="50"
                           min="0"
                           onChange={e => this.setState({ pitches: e })}
                           />

                        <span>Total Length of Pitches (m):</span>
                        <Incrementor
                           value={this.state.totalLength}
                           min="0"
                           onChange={e => this.setState({ totalLength: e })}
                           />

                        <span>Send Type: </span>
                        <SegmentedControl
                           options={sendTypes}
                           onSelect={selectedOption =>
                              this.setState({ sendType: selectedOption })
                           }
                           />

                        <span>Style:</span>
                        <SegmentedControl
                           options={climbStyle}
                           onSelect={selectedOption =>
                              this.setState({ climbStyle: selectedOption })
                           }
                           />

                        <span>Route Styles:</span>
                        <CheckboxGroup
                           options={routeStyles}
                           onChange={e => this.setState({ routeStyles: e })}
                           />

                        <span>Comments:</span>
                        <textarea
                           rows="5"
                           name="comment"
                           placeholder=" How did you like the climb?
                           What did you do well or can improve on?
                           Comments will be saved to your journal."
                           onChange={e => this.setState({ comment: e.target.value })}
                           >
                        </textarea>

                        <div className="buttons">
                           <button
                              className="cancel"
                              type="button"
                              onClick={e => this.props.onSubmit()}
                           >Cancel</button>
                           <button type="submit">Save</button>
                        </div>
                     </fieldset>
                  </Form>
               )}
               </Mutation>
            )}
         </Mutation>
      )
   }
}

export default AddClimb;
export { CREATE_CLIMB_MUTATION, CREATE_JOURNALENTRYFORCLIMB_MUTATION };