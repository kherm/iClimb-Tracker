import gql from "graphql-tag";
import { Query } from 'react-apollo';
import { adopt } from 'react-adopt';
import cloneDeep from 'lodash.clonedeep'
import { reduceGradeCounts, sendTypeEnum, getGradeCategoryLabels } from './Charts/ChartHelper';

const USER_TOT_VERTICAL_QUERY = gql`
   query USER_TOT_VERTICAL_QUERY {
      totalVertical {
         value
      }
   }
`;

const totalVertical = ({ render }) => (
   <Query query={USER_TOT_VERTICAL_QUERY}>
      {({ data }) => {
         let value = data.totalVertical.value;
         if (!value) { value = 0 }
         return render({ value })
      }}
   </Query>
);

const USER_PITCHES_MONTH_QUERY = gql`
   query USER_PITCHES_MONTH_QUERY {
      pitchesThisMonth {
         value
      }
   }
`;

const pitchesThisMonth = ({ render }) => {
   return (
      <Query query={USER_PITCHES_MONTH_QUERY}>
         {({ data }) => {
            let value = data.pitchesThisMonth.value;
            if (!value) { value = 0 }
            return render({ value })
         }}
      </Query>
   );
}

const USER_TOT_DAYS_YEAR_QUERY = gql`
   query USER_TOT_DAYS_YEAR_QUERY {
      totalDaysThisYear {
         value
      }
   }
`;

const totalDaysThisYear = ({ render }) => {
   return (
      <Query query={USER_TOT_DAYS_YEAR_QUERY}>
         {({ data }) => {
            let value = data.totalDaysThisYear.value;
            if (!value) { value = 0 }
            return render({ value })
         }}
      </Query>
   );
}

const USER_HIGHEST_GRADE_QUERY = gql`
   query USER_HIGHEST_GRADE_QUERY {
      highestRedpointGrade {
         grade
      }
   }
`;

const highestRedpointGrade = ({ render }) => (
   <Query query={USER_HIGHEST_GRADE_QUERY}>
      {({ data }) => {
         const value = data.highestRedpointGrade.grade;
         return render({ value })
      }}
   </Query>
);

const GRADES_VS_COUNTS_QUERY = gql`
   query GRADES_VS_COUNTS_QUERY {
      gradesChart {
         seriesData {
            sendType
            grade
            count
         }
      }
   }
`;

const gradesChart = ({ render }) => (
   <Query query={GRADES_VS_COUNTS_QUERY}>
      {({ data }) => {
         //consolodate all counts by grade
         const reducedGrades = data.gradesChart.map(grade => {
            const data = cloneDeep(grade.seriesData);
            return reduceGradeCounts(data);
         });
         //re-organize counts by sendType
         const organizedGradesBySendType = data.gradesChart.map((grade, i) => {
            let sendTypeData = [[], [], []];
            grade.seriesData.forEach(climb => {
               if (climb.sendType) {
                  sendTypeData[sendTypeEnum[climb.sendType]].push(climb);
               }
            });
            //add missing grades ( count is 0 ) to smooth out graph line
            const categories = getGradeCategoryLabels(i);
            sendTypeData.forEach(type => {
               if (type.length < 4) {
                  categories.forEach(grade => {
                     if (type.every(climb => climb.grade !== grade)) {
                        type.push({ grade, count: 0 });
                     }
                  })
               }
            });
            return sendTypeData;
         });

         return render({ reducedGrades, organizedGradesBySendType })
      }}
   </Query>
);

const STYLE_VS_TIME_QUERY = gql`
   query STYLE_VS_TIME_QUERY {
      climbStyleChart {
         seriesData {
            grade
            climbStyle
            routeStyles
            date
         }
      }
   }
`;

const climbStyleChart = ({ render }) => (
   <Query query={STYLE_VS_TIME_QUERY}>
      {({ data }) => {
         const climbStyles = data.climbStyleChart.map(page =>
            page.seriesData.map(climb => {
               climb.date = new Date(climb.date)
               return climb;
            })
         );
         return render({ climbStyles })
      }}
   </Query>
);

export const StatsContainer = adopt({
   totalVertical,
   totalDaysThisYear,
   pitchesThisMonth,
   highestRedpointGrade,
   gradesChart,
   climbStyleChart
});

export {
   USER_HIGHEST_GRADE_QUERY,
   USER_PITCHES_MONTH_QUERY,
   USER_TOT_DAYS_YEAR_QUERY,
   USER_TOT_VERTICAL_QUERY,
   GRADES_VS_COUNTS_QUERY,
   STYLE_VS_TIME_QUERY,
};
