import React from 'react';
import router from 'next/router';
import { Wrapper } from '../styles/GlobalStyles';
import AddButton from "../AddButton";
import StatItems from "./StatItems";

const StatList = (props) => (
   <Wrapper>
      <AddButton label="+ Climb" onClick={e => router.push('/Climb')} />
      <StatItems />
   </Wrapper>
);

export default StatList;