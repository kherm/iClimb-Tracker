import React from 'react';
import {
   VictoryChart,
   VictoryAxis,
   VictoryGroup,
   VictoryArea,
   VictoryLegend
} from 'victory';
import { Label, Container } from '../../styles/ChartStyles';
import { theme } from '../../styles/GlobalStyles';
import { getGradeCategoryLabels, sendTypeEnum } from './ChartHelper';

const SendtypeVsCount = (props) => {
   const page = props.page;
   const categories = getGradeCategoryLabels(page);
   const grades = props.data[page];

   // sets y axis to min 10 ticks
   let yaxis = false;
   if (grades.every(sendType =>
      sendType.every(grade => grade.count < 10))) {
      yaxis = true;
   }

   return (
      <Container>
         {grades.every(sendType => sendType.every(climb => climb.count === 0)) ?
            <Label>No climbs logged</Label> : <></>}

         <VictoryChart
            padding={{ top: 15, bottom: 50, right: 50, left: 50 }}
            domainPadding={5}
         >
            <VictoryAxis
               tickValues={categories}
            />
            <VictoryAxis
               dependentAxis
               domain={yaxis ? { y: [0, 9] } : null}
               label="Climb Count"
               tickFormat={(t) => Math.round(t) === t ? t : undefined}
            />
            <VictoryLegend
               x={90} y={20}
               orientation="horizontal"
               gutter={20}
               colorScale={[`${theme.yellow}`, `${theme.blue}`, `${theme.red}`]}
               data={[
                  { name: "Top Rope" }, { name: "Redpoint" }, { name: "Onsight" }
               ]}
            />
            <VictoryGroup
               style={{ data: { strokeWidth: 3, fillOpacity: 0.5 } }}
            >
               <VictoryArea
                  style={{ data: { fill: `${theme.yellow}`, stroke: `${theme.yellow}` } }}
                  data={grades[sendTypeEnum["Top Rope"]]}
                  x="grade"
                  y="count"
                  categories={{ x: categories }}
                  animate={{
                     duration: 500,
                     onLoad: { duration: 300 }
                  }}
                  interpolation="cardinal"
               />
               <VictoryArea
                  style={{ data: { fill: `${theme.blue}`, stroke: `${theme.blue}` } }}
                  data={grades[sendTypeEnum["Redpoint"]]}
                  x="grade"
                  y="count"
                  animate={{
                     duration: 500,
                     onLoad: { duration: 300 }
                  }}
                  interpolation="cardinal"
               />
               <VictoryArea
                  style={{ data: { fill: `${theme.red}`, stroke: `${theme.red}` } }}
                  data={grades[sendTypeEnum["Onsight"]]}
                  x="grade"
                  y="count"
                  animate={{
                     duration: 500,
                     onLoad: { duration: 300 }
                  }}
                  interpolation="cardinal"
               />

            </VictoryGroup>
         </VictoryChart>
      </Container>
   );
}

export default SendtypeVsCount;