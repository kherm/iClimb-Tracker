const sendTypeEnum = {
   "Top Rope": 0,
   "Redpoint": 1,
   "Onsight": 2
}

//returns an array of grade category labels based on page order
const getGradeCategoryLabels = (page) => {
   let categories = ["5.6", "5.7", "5.8", "5.9"];
   if (page > 0) {
      categories = categories.map((grade, i) =>
         `5.1${page - 1} ${String.fromCharCode(i + 97)}`
      );
   }
   return categories;
}

//reduces objects to single grade, consolidating counts
const reduceGradeCounts = (array) => {
   const newArray = [];
   array.forEach(obj => {
      let index;
      const gradeNotAdded = newArray.every((grade, i) => {
         if (obj.grade === grade.grade) {
            index = i;
            return false;
         }
         return true;
      });
      if (gradeNotAdded) {
         newArray.push(obj);
      } else {
         newArray[index].count += obj.count;
      }
   });
   return newArray;
};

export {
   sendTypeEnum,
   getGradeCategoryLabels,
   reduceGradeCounts,
};