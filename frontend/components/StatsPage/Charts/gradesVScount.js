import React from 'react';
import { VictoryBar, VictoryChart, VictoryAxis } from 'victory';
import { theme } from '../../styles/GlobalStyles';
import { Label, Container } from '../../styles/ChartStyles';
import { getGradeCategoryLabels } from './ChartHelper';

const GradeVSCount = (props) => {
   const grades = props.data;
   const page = props.page;
   const categories = getGradeCategoryLabels(page);

   //sets y axis to min 10 ticks
   let yaxis = false;
   if (grades[page].every(grade => grade.count < 10)) {
      yaxis = true;
   }
   return (
      <Container>
         {grades[page].length ? <></> : <Label>No climbs logged</Label>}
         <VictoryChart
            domainPadding={40}
            padding={{ top: 15, bottom: 50, right: 50, left: 50 }}
         >
            <VictoryAxis
               tickValues={categories}
            />
            <VictoryAxis
               dependentAxis
               domain={yaxis ? { y: [0, 9] } : null}
               label="Climb Count"
               tickFormat={(t) => Math.round(t) === t ? t : undefined}
            />
            <VictoryBar
               style={{ data: { fill: `${theme.green}` } }}
               data={grades[page]}
               x="grade"
               y="count"
               barWidth={60}
               animate={{
                  duration: 500,
                  onLoad: { duration: 300 }
               }}
            />
         </VictoryChart>
      </Container>
   )
};

export default GradeVSCount;