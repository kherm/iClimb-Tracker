import React, { Component } from 'react';
import styled from 'styled-components';
import {
   VictoryLine,
   VictoryChart,
   VictoryAxis,
   VictoryScatter,
} from 'victory';
import { Label, Container } from '../../styles/ChartStyles';
import { theme } from '../../styles/GlobalStyles';
import { sortGrades, grades } from '../../../lib/ClimbData';
import { formatDate, months } from '../../../lib/Date';

const InterpolationSelect = ({ currentValue, values, onChange }) => {
   const Select = styled.select`
      margin-left: 40%;
      font-size: 1.5rem;
   `;

   return (
      <Select onChange={onChange} value={currentValue} style={{ width: 100 }}>
         {values.map(value => <option value={value} key={value}>{value}</option>)}
      </Select>
   );
}

class ClimbStyleByGradeVsTime extends Component {
   constructor(props) {
      super(props);

      this.interpolations = this.props.interpolations;
      this.state = {
         interpolation: this.interpolations[0],
      }

   }

   render() {
      const page = this.props.page;
      const data = this.props.data[page];

      // get page year
      const year = data.length === 0 ? (new Date()).getFullYear() : data[0].date.getFullYear();
      // set x axis ticks to be the middle of each month
      const dateCategories = months.map((month, i) => new Date(year, i, 15))
      // get y axis tick categories
      const gradeCategories = sortGrades(data.map(climb => climb.grade));
      const firstGradesIndex = grades.findIndex(grade => grade === gradeCategories[0])
      const lastGradesIndex = grades.findIndex(grade => grade === gradeCategories[gradeCategories.length - 1])
      // data filtered by style
      const interpolationData = data.filter(climb => climb[this.props.filter].includes(this.state.interpolation));

      return (
         <Container>
            <InterpolationSelect
               currentValue={this.state.interpolation}
               values={this.interpolations}
               onChange={e => {
                  this.setState({ interpolation: e.target.value });
               }}
            />
            {interpolationData.length === 0 ? <Label>No climbs logged</Label> : <></>}

            <VictoryChart
               domainPadding={10}
               padding={{ top: 15, bottom: 50, right: 50, left: 50 }}
               scale={{ x: "time" }}
            >
               <VictoryAxis
                  label={year}
                  tickValues={dateCategories}
                  tickFormat={(t) => formatDate(t, "month").substr(0, 3)}
               />
               <VictoryAxis
                  dependentAxis
                  tickValues={grades.slice(firstGradesIndex, lastGradesIndex + 1)}
                  tickFormat={(t, i) => {
                     if (lastGradesIndex - firstGradesIndex < 5) {
                        return t
                     } else {
                        if (i % 4 === 0) return t;
                        else return undefined;
                     }
                  }}
               />
               {/* calculate linear regression before plotting */}
               {/* <VictoryLine
                  data={interpolationData}
                  x="date"
                  y="grade"
                  style={{ data: { stroke: `${theme.green}` } }}
                  animate={{
                     duration: 1000,
                     onLoad: { duration: 500 }
                  }}
               /> */}
               <VictoryScatter
                  data={interpolationData}
                  x="date"
                  y="grade"
                  size={2.5}
                  style={{ data: { fill: `${theme.blue}` } }}
               />
            </VictoryChart>
         </Container>
      );
   }
}

export default ClimbStyleByGradeVsTime;