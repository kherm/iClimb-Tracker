import styled from "styled-components";
import { theme } from "../styles/GlobalStyles";

const Item = styled.div`
  text-align: center;
  margin: 0;
`;

const Title = styled.h3`
  border: 1px solid black;
  padding: 1rem;
  margin: 0;
  background-color: ${theme.yellow};
`;

const Stat = styled.p`
  font-weight: bolder;
  font-size: 4.5rem;
  border: 1px solid black;
  margin: 0;
  @media(max-width: 420px) {
    font-size: 3.75rem;
  }
`;

const StatItem = props => {
  let data = props.data;
  if (props.data === 0) { data = "0" }
  else if (props.data === null) { data = "No Data" }
  return (
    <Item>
      <Title>{props.title || "Stat"}</Title>
      <Stat>{data}</Stat>
    </Item>
  );
}

export default StatItem;
