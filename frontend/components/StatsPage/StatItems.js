import React from "react"
import styled from "styled-components";
import { StatsContainer } from "./StatsContainer";
import StatItem from "./StatItem";
import Chart from "./Chart";
import GradeVSCount from './Charts/gradesVScount';
import SendtypeVsCount from './Charts/sendtypeVScount';
import ClimbStyleByGradeVsTime from './Charts/climbStyleByGradeVStime'
import { climbStyle, routeStyles } from '../../lib/ClimbData';

const Wrapper = styled.div`
   margin: 1rem 0;
`;

const StatHighlights = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 1fr 1fr;
  grid-gap: 1rem;
`;

const ChartSet = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr;
  grid-gap: 20px;
  margin: 2rem 0;
`;

const StatItems = (props) => {
   return (
      <StatsContainer>
         {({
            totalVertical,
            totalDaysThisYear,
            pitchesThisMonth,
            highestRedpointGrade,
            gradesChart,
            climbStyleChart,
         }) => {
            return (
               <Wrapper>
                  <StatHighlights>
                     <StatItem
                        title="Total Vertical (m)"
                        data={totalVertical.value}
                     />
                     <StatItem
                        title="Highest Redpoint"
                        data={highestRedpointGrade.value}
                     />
                     <StatItem
                        title="Total Days this Year"
                        data={totalDaysThisYear.value}
                     />
                     <StatItem
                        title="Pitches this Month"
                        data={pitchesThisMonth.value}
                     />
                  </StatHighlights>

                  <ChartSet>
                     <Chart title="Volume by Grade" maxPage={gradesChart.reducedGrades.length}>
                        {page =>
                           <GradeVSCount
                              page={page}
                              data={gradesChart.reducedGrades}
                           />
                        }
                     </Chart>
                     <Chart title="Volume by Send" maxPage={gradesChart.organizedGradesBySendType.length}>
                        {page =>
                           <SendtypeVsCount
                              page={page}
                              data={gradesChart.organizedGradesBySendType}
                           />
                        }
                     </Chart>
                     <Chart title="Volume over Time" maxPage={climbStyleChart.climbStyles.length}>
                        {page =>
                           <ClimbStyleByGradeVsTime
                              page={page}
                              data={climbStyleChart.climbStyles}
                              filter="climbStyle"
                              interpolations={climbStyle}
                           />
                        }
                     </Chart>
                     <Chart title="Volume over Time" maxPage={climbStyleChart.climbStyles.length}>
                        {page =>
                           <ClimbStyleByGradeVsTime
                              page={page}
                              data={climbStyleChart.climbStyles}
                              filter="routeStyles"
                              interpolations={routeStyles}
                           />
                        }
                     </Chart>
                  </ChartSet>
               </Wrapper>
            );
         }}
      </StatsContainer>
   );
}

export default StatItems;