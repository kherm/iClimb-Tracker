import React, { Component } from "react";
import styled from "styled-components";
import { GlobalStyle, theme } from "./styles/GlobalStyles";
import Header from "./Header";
import Nav from "./Nav"
import Meta from "./Meta";

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  min-height: 100vh;
  justify-content: space-between;
  #NavExtension {
    background-color: ${theme.green};
    height: 90px;
    z-index: 0;
  }
`;

const Inner = styled.main`
  display: grid;
  justify-content: center;
  max-width: ${theme.maxWidth};
  margin: 0 auto;
  margin-top: -10rem;
  padding: 2rem;
  height: 100%;
  z-index: 10;
  @media (max-width: 450px) {
    padding: 0.5rem;
  }
`;

export default class Page extends Component {
  render() {
    const { children } = this.props;

    return (
      <Wrapper>
        <Meta />
        <Header>
          <Nav />
          <div id="NavExtension"></div>
        </Header>
        <Inner>
          {children}
        </Inner>
        <GlobalStyle />
        <Footer />
      </Wrapper>
    );
  }
}

const Foot = styled.div`
  display: grid;
  place-items: center;
  background-color: ${theme.green};
  height: 100px;
  align-self: end;
  img {
    height: 100px;
  }
`;

const Footer = props => (
  <Foot>
    <img className="logo" src="../static/mountain.png" alt="logo" />
  </Foot>
);
