import styled from 'styled-components';

const SelectStyle = styled.select`
  font-size: 1.75rem;
  border-radius: 10px;
  padding: 1rem;
  margin-bottom: 1rem;
`;

const Select = props => (
  <SelectStyle name="selectedGrade" defaultValue={props.preSelect}>
    {props.options.map(grade => (
      <option
        key={grade}
        value={grade}
        onClick={() => props.onSelect(grade)}
      >
        {grade}
      </option>
    )
    )}
  </SelectStyle>
);

export default Select;
