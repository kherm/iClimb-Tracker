import styled, { css } from 'styled-components';
import { theme } from './GlobalStyles';

const Form = styled.form`
   display: grid;
   justify-content: center;

   fieldset {
      display: grid;
      grid-gap: 12px;
      background: white;
      padding: 1rem;
      border: none;
      border-radius: 50px;

      h2 {
         text-align: center;
      }
      span {
         margin-left: 0.5rem;
      }
      input, select, textarea {
         font-size: 1.75rem;
         padding: 1rem;
         border: none;
         border-bottom: 1px solid grey
      }
      button {
         margin: 1rem 0;
         padding: 1rem;
         border-color: ${theme.green};
         border-width: 1px;
         box-shadow: 1px 1px grey;
         border-radius: 10px;
         background-color: ${theme.blue};
         color: ${theme.yellow};
         font-weight: bold;
         font-size: 1.75rem;
         letter-spacing: 1px;
         cursor: pointer;
      }
      .note {
         font-style: italic;
      }

      ${props => props.addClimb && css`
         grid-gap: 5px;
         span {
            font-size: 1.25rem;
         }
         select, input, textarea {
            border: 1px solid grey;
            border-radius: 10px;
            margin-bottom: 1rem;
         }
         input[type="date"], textarea {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: inherit;
         }
         .buttons {
            margin-top: 1rem;
            display: flex;
            * {
               flex-grow: 1;
               margin: 0 0.5rem;
            }
            .cancel {
               background-color: rgba(255, 0, 0, 0.75);
               flex-grow: 0.5;
            }
         }
      `};
   }
`;

export default Form;