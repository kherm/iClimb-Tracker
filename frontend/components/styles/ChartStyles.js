import styled from 'styled-components';

const Label = styled.p`
   position: absolute;
   text-align: center;
   top: 30%;
   font-size: 2rem;
   font-style: italic;
   padding: 1rem;
`;

const Container = styled.div`
   position: relative;
`;

export { Label, Container };