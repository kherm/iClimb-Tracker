import styled from 'styled-components';
import { createGlobalStyle } from "styled-components";

const theme = {
  black: "#393939",
  green: "#253A3B",
  yellow: "#FFF25E",
  blue: '#00699d',
  white: "#FFF8AB",
  red: "#AB1120",
  maxWidth: "1000px"
};

const GlobalStyle = createGlobalStyle`
    @font-face {
      font-family: 'radnika_next';
      src: url('/static/radnikanext-medium-webfont.woff2')
      format('woff2');
      font-weight: normal;
      font-style: normal;
    }
    html {
      box-sizing: border-box;
      font-size: 10px;
    }
    /*best way for inheriting box-sizing it set it globally and inherit*/
    *, *:before, *:after {
      box-sizing: inherit;
    }
    body {
      background: ${theme.white};
      color: ${theme.black};
      padding: 0;
      margin: 0;
      font-size: 1.5rem;
      line-height: 2;
      font-family: 'radnika_next';
    }
    a {
      text-decoration: none;
      color: ${theme.black};
    }
  `;

const Wrapper = styled.div`
   display: grid;
   justify-items: center;
   background: white;
   padding: 1.2rem;
   border-radius: 50px;
   width: 60vw;
   * {
     width: 100%;
   }

   @media(max-width: 700px) {
      width: 90vw;
      * {
        width: 99%;
      }
   }
`;

export { GlobalStyle, theme, Wrapper };
