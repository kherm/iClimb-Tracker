import styled from "styled-components";
import { theme } from './GlobalStyles';

const ListStyle = styled.div`
  display: grid;
  justify-items: center;
  margin: 1rem 0;
  padding: 1rem;
  /* border-bottom: 2px solid grey; */
  border-top: 2px solid grey;

  h3 {
    text-align: center;
    /* border: 3px solid ${theme.blue}; */
    /* background-color: ${theme.yellow}; */
    padding: 1rem;
    margin: 1rem 0;
    /* transform: skew(-15deg); */
  }

  ol {
    /* min-width: 330px;
    max-width: ${theme.maxWidth}; */
    margin: 0;
  }
  .completed {
    list-style-type: disc;
  }
  .note {
    font-style: italic;
    color: ${theme.green};
    text-align: center;
  }

`;

const List = styled.li`
  padding-left: 1rem;

  .completed {
    position: relative;
    * {
      color: darkgrey !important;
    }
  }

  .completed::before {
    content: '';
    border-bottom: 2px solid grey;
    width: 100%;
    position: absolute;
    right: 0;
    top: 25%;
  }
`;

const ItemStyle = styled.div`
  display: grid;
  grid-template-columns: 5fr 1fr;
  grid-gap: 10px;
  align-items: start;
  margin-bottom: 1rem;
  cursor: pointer;

  a {
    display: grid;
    grid-template-columns: 3fr 2fr;
    :hover {
      color: black;
      text-shadow: 0.5px 0.5px ${theme.blue};
    }
  }
  p {
    margin: 0;
  }
  input[type="checkbox"] {
    margin-top: 1rem;
  }
  .detail {
    font-size: 1.2rem;
    color: grey;
  }
  .gradeCol {
    text-align: right;
  }
`;

export {
  ListStyle,
  List,
  ItemStyle
};