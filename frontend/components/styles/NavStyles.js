import styled from "styled-components";
import { theme } from "./GlobalStyles";

const NavList = styled.ul`
  display: flex;
  justify-content: space-around;
  margin-top: 0;
  a,
  button {
    display: inline-block;
    color: ${theme.yellow};
    padding: 0.75rem 1rem;
    background: 0;
    border: 0;
    font-size: 18px;
    font-weight: bold;
    cursor: pointer;
    opacity: 0.75;
    @media (max-width: 700px) {
      font-size: 15px;
      padding: 0 10px;
    }
    &:after {
      content: '';
      display: block;
      width: 0;
      height: 3px;
      background: ${theme.yellow};
      transition: width 0.4s;
    }
    &:hover::after {
      width: 100%;
      transition: width .3s;
    }
  }
  .active {
    opacity: 1;
  }
`;

export { NavList };
