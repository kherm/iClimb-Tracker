import React from "react";
import styled from "styled-components";
import Router from "next/router";
import Link from 'next/link';
import NProgress from "nprogress";
import { theme } from "./styles/GlobalStyles";
import User from './User';

Router.onRouteChangeStart = () => {
  NProgress.start();
};
Router.onRouteChangeComplete = () => {
  NProgress.done();
};
Router.onRouteChangeError = () => {
  NProgress.done();
}

const StyledHeader = styled.header`
  background-color: ${theme.green};
  display: grid;
  grid-template-columns: 0.25fr 1fr 0.25fr;
  * {
    grid-column: 2 / 2;
  }
  #titleLogo {
    transform: skew(-15deg);
    * {
      text-align: center;
    }
  }
  p {
    color: ${theme.yellow};
    margin: 0;
    padding: 0;
    transform: translate(5%, -170%);
  }
`;

const Title = styled.h1`
  font-size: 4rem;
  color: ${theme.yellow};
  margin: 2rem 0;
  letter-spacing: 1px;
  img {
    width: 6rem;
    transform: translate(-20%, 20%);
  }
`;

const Header = (props) => {
  return (
    <User>
      {({ data: { me } }) => {
        let href = '/index';
        if (me) { href = '/stats' }
        return (
          <StyledHeader>
            <Link href={href}>
              <a id="titleLogo">
                <Title>
                  iClimb
            <img src="../static/mountain.png" alt="logo" />
                </Title>
                <p>Tracker</p>
              </a>
            </Link>
            {props.children}
          </StyledHeader>
        )
      }}
    </User>
  );
};

export default Header;
