import styled from "styled-components";
import PropTypes from 'prop-types';
import { theme } from "./styles/GlobalStyles";

const Button = styled.button`
  font-size: ${props => props.fontSize};
  font-weight: bolder;
  padding: 1rem 1.75rem;
  border-radius: 100px;
  border: 0;
  box-shadow: 1px 1px 1px ${theme.green};
  cursor: pointer;
  background-color: ${props => props.backgroundColor};
  a {
    text-shadow: 1px 1px 1px ${theme.green}, -1px -1px 1px ${theme.green};
    color: ${theme.yellow};
  }
  @media(max-width: 700px) {
    font-size: 1.5rem;
  }
`;

const AddButton = props => (
  <Button
    fontSize={props.fontSize || "2rem"}
    backgroundColor={props.backgroundColor || `${theme.blue}`}
    onClick={props.onClick}
    type={props.type || ""}
    className={props.className || ""}
  >
    <a>{props.label}</a>
  </Button>
);

AddButton.propTypes = {
  onClick: PropTypes.func,
  label: PropTypes.string,
};

export default AddButton;
