import PleaseSignIn from "../components/PleaseSignIn";
import JournalList from "../components/JournalPage/JournalList";

const Journal = props => (
  <PleaseSignIn>
    <JournalList />
  </PleaseSignIn>
);

export default Journal;
