import PleaseSignIn from '../components/PleaseSignIn';
import StatList from '../components/StatsPage/StatList';

const Stats = props => (
  <PleaseSignIn>
    <StatList />
  </PleaseSignIn>
);

export default Stats;
