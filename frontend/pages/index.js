import React from 'react';
import styled from 'styled-components';
import PleaseSignIn from '../components/PleaseSignIn';
import Toggleable from '../components/Toggleable';
import AddButton from '../components/AddButton';

const Banner = styled.div`
  margin: 2rem 0;
  display: grid;
  grid-template-columns: 1fr 1fr;
  align-items: center;
  justify-items: center;
  .logo {
    width: 20%;
  }
  img {
    width: 50%;
  }
  h2 {
    text-align: center;
  }
  @media (max-width: 700px) {
    grid-template-columns: 1fr;
  }
`;

const Headlines = styled.div`
  padding: 2rem;
`;

const Wrapper = styled.div`
  display: grid;
  button {
    justify-self: center;
    padding: 1.5rem 4rem;
  }
`;
const Login = () => (
  <div>
    <Toggleable>
      {(show, toggle) => (
        <Wrapper>
          {show ? <></> :
            <AddButton onClick={toggle} label="Login or Register" />
          }
          {show ?
            <PleaseSignIn>
              <div></div>
            </PleaseSignIn> : <></>
          }
        </Wrapper>
      )}
    </Toggleable>

    <Banner>
      <Headlines>
        <h2>Climb.</h2>
      </Headlines>
      <img src="../static/icons/climb.png" alt="stats" />
    </Banner>
    <Banner>
      <img src="../static/icons/data.png" alt="data" />
      <Headlines>
        <h2>Enter your data.</h2>
      </Headlines>
    </Banner>
    <Banner>
      <Headlines>
        <h2>Log your projects.</h2>
      </Headlines>
      <img src="../static/icons/mountain.png" alt="project" />
    </Banner>
    <Banner>
      <img src="../static/icons/progress.png" alt="attempt" />
      <Headlines>
        <h2>Watch your progress.</h2>
        <h2>Get motivated.</h2>
      </Headlines>
    </Banner>
    <Banner>
      <Headlines>
        <h2>Document your climbing.</h2>
        <h2>Achieve your goals.</h2>
      </Headlines>
      <img src="../static/icons/journal.png" alt="journal" />
    </Banner>
  </div>
);

export default Login;