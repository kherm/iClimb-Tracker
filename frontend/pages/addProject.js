import router from 'next/router';
import AddProject from '../components/ProjectsPage/AddProject';

const addProject = () => (
   <div>
      <AddProject onSubmit={e => router.push('/projects')} />
   </div>
);

export default addProject;