import SingleProject from '../components/ProjectsPage/SingleProject';

const Project = props => (
   <div>
      <SingleProject id={props.query.id} />
   </div>
);

export default Project;