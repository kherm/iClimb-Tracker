import EditEntry from '../components/JournalPage/EditEntry';

const JournalEntry = props => (
   <EditEntry id={props.query.id} />
);

export default JournalEntry;