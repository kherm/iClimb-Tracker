import router from 'next/router';
import AddClimb from '../components/StatsPage/AddClimb';

const Climb = () => (
   <div>
      <AddClimb onSubmit={e => router.push('/stats')} />
   </div>
);

export default Climb;
