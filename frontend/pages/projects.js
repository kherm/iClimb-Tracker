import PleaseSignIn from "../components/PleaseSignIn";
import RouteList from "../components/ProjectsPage/RouteList";

const Projects = props => (
  <PleaseSignIn>
    <RouteList />
  </PleaseSignIn>
);

export default Projects;